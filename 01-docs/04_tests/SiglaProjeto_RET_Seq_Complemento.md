![float](../../01_docs/00_imagem_template/Produs_git.png)

## PRODUS - Processo de Desenvolvimento Unificado de *Software*
### RET - Resultado de Execução de Teste

<br/>

## 1. Identificação da Iniciativa

  - **Iniciativa de Teste:** [Nome da iniciativa]

  - **Iteração:**

<!-- end list -->

<!--
    Número da iteração de teste do _software_
-->

  - **Ciclo de Teste:**

<!-- end list -->

<!--
    Identificação do ciclo de testes referente à iteração de teste do _software_
-->

  - **Roteiro /Suíte de Teste:**

<!-- end list -->

<!--
    Descreva o nome do roteiro ou suíte de teste, no qual as evidências estão relacionadas
-->

  - **Analista de Teste:**

<!-- end list -->

<!--
    Indique o nome do profissional responsável pela liderança técnica dos testes
-->

  - **Testador:**

<!-- end list -->

<!--
    Indique o nome do profissional responsável pela execução dos testes
-->

## 2. Finalidade

A finalidade deste documento é organizar e apresentar uma análise resumida dos resultados do teste e as principais medidas do teste para revisão e avaliação, geralmente executadas pelos principais envolvidos.

## 3. Resumo Executivo

### 3.1. Dados do teste

  - **Data de início do teste:**

<!-- end list -->

<!--
    DD/MM/AAA – HH : MM
-->

  - **Data de fim do teste:**

<!-- end list -->

<!--
    DD/MM/AAA – HH : MM
-->

  - **Versão testada (*baseline*):**

<!-- end list -->

<!--
    X.X – versão do sistema testado
-->

  - **Local do teste:**

<!-- end list -->

<!--
    Indicar o local da execução do teste
-->

  - **Ambiente do teste utilizado:**

<!-- end list -->

<!--
    Descreva em linhas gerais o ambiente utilizado para a execução dos testes
-->

  - **Descrição do teste:**

<!-- end list -->

<!--
    Descreva em linhas gerais o objetivo de observação do teste
-->

  - **Envolvidos:**

<!-- end list -->

<!--
    Indique o nome do(s) profissional(is) acionados e/ou envolvidos na execução dos testes e, se possível, a área a que são vinculados
-->

### 3.2. Números do teste

| **ESPECIFICAÇÃO de Teste**                      |        |
| ----------------------------------------------- | ------ |
| **Casos de testes criados antes do teste:**     | [30]   |
| **Casos de testes criados durante o teste:**    | [06]   |
| **Casos de testes que necessitam de correção**: | [02]   |


| **Teste de CONFIRMAÇÃO**                                  |        |
| --------------------------------------------------------- | ------ |
| **Casos de testes atribuídos para teste de confirmação:** | [10]   |
| - Casos de teste com defeitos persistentes:               | [02]   |
| - Casos de testes encerrados (efetivamente corrigidos):   | [08]   |


| **EXECUÇÃO de Teste**                         |        |
| --------------------------------------------- | ------ |
| **Casos de teste não executados/bloqueados:** | [5]    |
| **Casos de testes executados:**               | [95]   |
| - Casos de teste com erro:                    | [40]   |
| - Casos de teste com sucesso:                 | [55]   |

  

### 3.3. Percentuais do teste

| **EXECUÇÃO de Teste**                         |         |
| --------------------------------------------- | ------- |
| **Casos de teste não executados/bloqueados:** | [05%]   |
| **Casos de testes executados:**               | [95%]   |
| - Casos de teste com erro:                    | [40%]   |
| - Casos de teste com sucesso:                 | [55%]   |

## 4. Laudo da execução do teste

<!--
    Resuma brevemente os resultados do teste observados durante a execução
    
    Exemplos:
    Os casos de teste definidos para o ciclo de teste foram executados seguindo a abordagem e estratégia de teste, conforme definido no plano de teste. Os defeitos identificados na execução dos testes foram registrados e todos os defeitos, inclusive os de severidade crítica ou bloqueantes foram designados ao responsável pelo produto para correção.
    A *cobertura de teste*, em termos de cobertura dos casos de uso e de casos de teste, definidos no plano de teste, foi 95% concluída. 10 casos de teste que envolvem a operação de integração de  sistema não foram concluídos devido a erros no _software_ simulador ou indisponibilidade do outro _software_.
    A *análise dos defeitos* indica que a maioria dos defeitos encontrados tendem a ser problemas menores classificados com severidade normal ou pequena. Outra descoberta significativa foi a de que os componentes de _software_ que constituem a interface com o sistema Y continham o número mais alto de defeitos.
    A *análise dos defeitos* indica que a maioria dos defeitos encontrados tende a ser problemas grandes classificados como bloqueantes ou críticos em termos de severidade. A outra descoberta significativa foi que os componentes de software que compõem a interface para o Sistema Alfa continha um número significativo de defeitos.
-->

<br/>  
  

## Anexo I

### Visão Geral dos Casos de Teste

| **Versão testada** (*baseline*)     | **Roteiro/Suíte de Teste**                                | **Caso de Teste**                                       | **Plataforma**                                                                            | ***Status***                                                               | **Comentários** | **Bugs**        |
| ----------------------------------- | --------------------------------------------------------- | ------------------------------------------------------- | ----------------------------------------------------------------------------------------- | -------------------------------------------------------------------------- | --------------- | --------------- |
| [*0.1 – Identificação da versão*]   | [*ID da Suíte de Teste - Nome da Suíte de Teste*]         | [*ID do Caso de Teste - Nome do Caso de Teste*]         | [*Web browser, ou sistema operacional, ou dispositivo de hardware, ou de configuração*]   | [*Executado com sucesso; Executado com falha; Bloqueado; Não executado*]   | Com1            | [*ID do Bug*]   |
| VT2                                 | RST2                                                      | CT2                                                     | Pla2                                                                                      | Sta2                                                                       | Com2            | Bugs2           |
| VT3                                 | RST3                                                      | CT3                                                     | Pla3                                                                                      | Sta3                                                                       | Com3            | Bugs3           |
| VT4                                 | RST4                                                      | CT4                                                     | Pla4                                                                                      | Sta4                                                                       | Com4            | Bugs4           |
| VT5                                 | RST5                                                      | CT5                                                     | Pla5                                                                                      | Sta5                                                                       | Com5            | Bugs5           |
| VT6                                 | RST6                                                      | CT6                                                     | Pla6                                                                                      | Sta6                                                                       | Com6            | Bugs6           |
| VT7                                 | RST7                                                      | CT7                                                     | Pla7                                                                                      | Sta7                                                                       | Com7            | Bugs7           |

  
> SEPROP <<seprop@tse.jus.br>> Sobre o *template* Markdown - RET: versão 1.0/ 02.03.2018 Url do *template* de referência: <http://sticonhecimento.tse.jus.br/cogti/seprop/templates/modelos-de-documentos-do-produs>
