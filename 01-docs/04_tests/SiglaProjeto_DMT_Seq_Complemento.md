![float](../../01_docs/00_imagem_template/Produs_git.png)

## PRODUS - Processo de Desenvolvimento Unificado de *Software*
### DMT - Demanda de Teste

<br/>  

## 1. Identificação da Iniciativa

  - **Iniciativa de Teste:** [Nome da iniciativa]

  - **Áreas Solicitantes do Teste:** [Unidade(s) Organizacional(is) do TSE que solicitou(ram) os serviços de testes]

  - **Data da Demanda:** [dd/mm/aaaa]

## 2. Escopo de teste

<!--
    Defina o escopo do teste, ou seja, as características do sistema, as funções a serem testadas e/ou suas fronteiras, considerando a aplicação que será avaliada. O escopo pode ser todo o sistema, uma parte dele, um caso de uso ou uma história de usuário.
-->

## 3. Demanda de teste

<!--
    Apresente as informações coletadas pela área de teste junto à área solicitante acerca da demanda.
-->

## 4. Recursos de ambiente

<!--
    Descreva os recursos de ambiente para o teste, ou seja, onde será executada a demanda de teste.
-->

## 5. Observações importantes

<!--
    Observações relevantes sobre a demanda.
-->

## 6. Referências

<!--
    Essa subseção deve fornecer uma lista completa de todos os documentos referenciados em qualquer ponto da demanda de teste. Especificar as fontes em que as referências podem ser obtidas.
-->

> SEPROP <<seprop@tse.jus.br>> Sobre o *template* Markdown - DMT: versão 1.0/ 02.03.2018 Url do *template* de referência: <http://sticonhecimento.tse.jus.br/cogti/seprop/templates/modelos-de-documentos-do-produs>
