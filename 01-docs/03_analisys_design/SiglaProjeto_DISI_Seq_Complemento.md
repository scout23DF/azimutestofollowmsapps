![float](../../01_docs/00_imagem_template/Produs_git.png)

## PRODUS - Processo de Desenvolvimento Unificado de *Software*
### DISI - Documento de Implantação de *Software* e Infraestrutura

<br/> 

<!--
    Nota 1: Este modelo foi criado para auxiliar na documentação da arquitetura a ser adotada nos sistemas de _software_.]
    Nota 2: Se achar conveniente, acrescente um diagrama UML para ilustrar melhor o cenário de algum tópico deste documento.
-->

## 1. Introdução

Este documento apresenta uma visão detalhada da implantação do sistema e da infraestrutura necessária para essa implantação.

## 2. Identificação dos responsáveis técnicos do sistema

  - **Coordenadoria / Seção Responsável pela Gerência do Sistema:** **SiglaCoordenadoria/SiglaSeção**

  - **Responsáveis técnicos:**

<!-- end list -->

<!-- Tabela -->

| **Nome**                                          | **Seção**        | ***E-mail*** |
| ------------------------------------------------- | ---------------- | ------------ |
| [*Equipe técnica de desenvolvimento – pessoas*] | [*SiglaSeção*] | [*E-mail*] |
| [*Equipe técnica de desenvolvimento – pessoas*] | [*SiglaSeção*] | [*E-mail*] |
| [*Equipe técnica de desenvolvimento – pessoas*] | [*SiglaSeção*] | [*E-mail*] |
| [*Equipe técnica de desenvolvimento – pessoas*] | [*SiglaSeção*] | [*E-mail*] |
| [*Equipe técnica de desenvolvimento – pessoas*] | [*SiglaSeção*] | [*E-mail*] |
| [*Equipe técnica de desenvolvimento – pessoas*] | [*SiglaSeção*] | [*E-mail*] |

## 3. Visão Tecnológica

<!--
    Preencher um bloco de informações para cada módulo/subsistema do sistema, caso haja diferença entre eles
-->

  - **Módulo / Subsistema:** **Nome do módulo ou subsistema**

  - **Plataforma:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    [Web
    Standalone
    Cliente/Servidor
-->

  - **Linguagem(ns) /Scripts de Programação Utilizada(s):**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Delphi - Versão: x.x
    PL/SQL - Versão: x.x
    Java - Versão: x.x
    Python - Versão: x.x
    Outra: xxxxx
-->

  - **Banco(s) de Dados:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Oracle - Versão: x.x
    MySQL - Versão: x.x
    Postgres SQL - Versão: x.x
    SQL Server - Versão: x.x
    Outro: xxxxx
-->

## 4. Visão de Ambiente

  - **Tipo de Aplicação:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Aplicação centralizada no TSE
    Aplicação centralizada em TRE
    Aplicação distribuída nos TREs e TSE
    Aplicação na estação de trabalho do usuário
-->

### 4.1 Ambiente de Servidor

<!--
    Caso haja diferenças arquiteturais e tecnológicas entre os módulos/subsistemas, para cada um deles preencha um bloco como o abaixo
    O bloco apresenta segmentação de acordo com a tecnologia utilizada no sistema
    
    Atenção: Os segmentos relativos às tecnologias não utilizadas deverão *ser excluídos* durante o preenchimento.
-->

  - **Módulo / Subsistema:** **Nome do módulo ou subsistema**

  - **Sistema(s) operacional(is) necessário(s) para execução (no servidor):**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação da versão
    Windows - Versão: x.x
    Linux Red Hat - Versão: x.x
-->

  - **Utiliza ODIN?**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Sim
    Não se aplica, até então
-->

  - **Utiliza LDAP?**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Servidor LDAP corporativo (MS-AD)
    Não se aplica, até então.
-->

  - **Utiliza FTP?**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Sim. Especifique: Nome do servidor
    Não se aplica, até então
-->

  - **Realiza envio de e-mail?**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Sim, servidor Corporativo TSE
    Sim, outro servidor, especifique: Nome do servidor
    Não se aplica, até então
-->

<!--
    Atenção: Os segmentos relativos às tecnologias *não utilizadas* deverão *ser excluídos* durante o preenchimento.
-->

  - Se a tecnologia utilizada for **JAVA** e/ou seus derivados, informe as *frameworks* e bibliotecas utilizadas:
    
   - **Servidor de Aplicação Alvo:**

    <!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    JBoss EAP - Versão: x.x
    Outro: xxxxx
    Não se aplica
    -->

   - **Cache Estático:**


<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Não utiliza
    Oracle WebCache
    Varnish
-->

  - Se a tecnologia utilizada for **Zope / Plone**, informe as *frameworks* e bibliotecas utilizadas:
    
      - **Forma(s) de Conexão com o Banco:**

    <!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    ZEO (Zope Enterprise Objects)
    Outra: xxxxx
    -->

    - **Demais Utilitários / *Plugins*:**

    <!--
    Informe os utilitários e _plugins_
    -->

    - **Servidor de Aplicação Alvo:**

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Zope - Versão: x.x
    Outro: xxxxx
-->

  - Se a tecnologia utilizada for **DELPHI**, informe as *frameworks* e bibliotecas utilizadas:
    
      - ***Frameworks* / Base:**

    <!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    TSE - FrameDelphi
    Outras: xxxxx
    -->

    - ***Frameworks* / Camada de Persistência:**

    <!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    ADO
    Outras: xxxxx
    -->

    - **Controle Transacional e Integração:**

    <!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    TSE - FrameDelphi
    Outras: xxxxx
    -->

    - ***Frameworks* Camada de Apresentação:**

    <!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    TSE - FrameDelphi
    Outras: xxxxx
    -->

    - **Demais *Framewoks* e Componentes:**

    <!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    TSE - ACESSO
    Outras: xxxxx
    -->

### 4.2 Ambiente de Cliente

<!--
    Caso haja diferenças arquiteturais e tecnológicas entre os módulos/subsistemas, preencha o bloco abaixo para cada um deles
-->

  - **Módulo / Subsistema:** **Nome do módulo ou subsistema**

  - ***Hardware*:**

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Memória RAM Instalada: xxxxx] MB
    Espaço em disco disponível: xxxxx] GB
-->

  - **Acessibilidade e segurança em pastas e registro:**
    
      - **Pastas:**
        
          - **Arquivos:**

        <!--
    Informe os arquivos
        -->

        - **Tipo de permissão:**

    <!--
    Remova os colchetes da alternativa correspondente
    Leitura
    Escrita
    -->

    - **Registro do Windows:**
    
        - **Chave:**

        <!--
        Informa a chave
        -->

        - **Valor:**

        <!--
        Informar o valor
        -->

        - **Tipo de permissão:**

<!--
    Remova os colchetes da alternativa correspondente
    Leitura
    Escrita
-->

  - ***Plugins* necessários para execução do *software*:**


<!--
    ex.: Applet Java, Flash, etc
-->

  - ***Softwares* adicionais::**

<!--
    ex.: MS Word, MS Excel, MS Access, BrOffce, etc.
-->

  - ***Hardware* adicional:**

<!--
    ex.: 2 monitores, scanner, leitor de código de barras, etc.
-->

  - **Sistema(s) operacional(is) necessários para execução (no cliente):**

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    MacOS - Versão: x.x
    Windows - Versão: x.x
    Solaris - Versão: x.x
    Linux - Versão: x.x
    Outro, especifique: xxxx
-->

  - ***Browser(s)*:**

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Internet Explorer - Versão: x.x
    Google Chrome - Versão: x.x
    Mozila Firefox - Versão: x.x
    Não se aplica, até então.
    Outro, especifique: xxxx
-->

  - **Haverá necessidade de instalação de algum *plugin*, agente, cliente de *software* (BDE, Oracle) nas estações de trabalho?**

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Não
    Sim. Quais: xxxxxxxxx
-->

## 5. Diagrama de Implantação

<!--
    Inserir imagem do diagrama de implantação utilizando a pasta 'Imagens' localizada dentro de 03_analise_projeto
-->

![float](Imagens/DISI_diagrama-implantacao.jpg)

<!--
    Figura 1 – Diagrama de Implantação
-->

<!--
    Identificar todos os elementos que compõem a aplicação em questão e explicá-los sucintamente
    Exemplo: use * antes do texto para exibição em modo de lista
    sico.war: módulo web que corresponde a toda aplicação;
    Intranet TSE: módulo acessado apenas da rede interna da Justiça Eleitoral.
-->

## 6. Configurações

<!--
    Indicar as configurações necessárias para funcionamento do sistema.
-->

### 6.1 Servidor de aplicações

<!--
    Configurações no servidor de aplicações, de acordo com a tecnologia
    
    Atenção: Os blocos relativos às tecnologias *não utilizadas* deverão *ser excluídos* durante o preenchimento.
-->

Se a tecnologia utilizada for **JAVA**:

<!--
    Bloco 1
-->

  - ***Datasource*:**
    
      - **Tipo:**

    <!--
    É obrigatório selecionar pelo menos uma opção, quando utilizar _datasource_
    Remova os colchetes da alternativa correspondente
    Desenvolvimento
    Homologação
    Simulado
    Treinamento
    Produção
    -->

    - **JNDI *Name*:**

    <!--
    Informar JNDI _Name_
    -->

    - **Usuário de banco:**

    <!--
    Identificar usuário de banco
    -->

    - **Nível de Isolamento da Transação:**

    <!--
    TRANSACTION_NONE
    TRANSACTION_READ_COMMITTED
    TRANSACTION_READ_UNCOMMITTED
    TRANSACTION_REPEATABLE_READ
    TRANSACTION_SERIALIZABLE
    -->

    - **Banco de Dados:**

<!--
    É obrigatório ao menos um, quando utilizar _datasource_
    Remova os colchetes da(s) alternativa(s) correspondente(s) e complemente a informação quando necessário
    Produção: Nome do servidor e porta
    Desenvolvimento: Nome do servidor e porta
    Homologação: Nome do servidor e porta
-->

<!--
    Bloco 2
-->

  - **JMS:**
    
    - ***Queue-Name*:**

    <!--
    Informar _Queue-Name_
    -->

    - **JNDI *Name*:**

<!--
    Informar JNDI _Name_
-->

<!--
    Bloco 3
-->

  - **Variáveis de ambiente:**

<!--
    Tabela
-->

| **Nome da variável**              | **Valor**                                  |
| --------------------------------- | ------------------------------------------ |
| [*br.jus.tse.leiaute.estatico*]   | [<http://cdn.tse.jus.br/estatico/final>]   |
| [*NV2*]                           | [*Valor2*]                                 |
| [*NV3*]                           | [*Valor3*]                                 |
| [*NV4*]                           | [*Valor4*]                                 |

<!--
    Bloco 4
-->

  - **Utiliza serviço de *e-mail*:**
    
      - **Servidor de *e-mail*:**

    <!--
    Indicar apenas o nome do servidor de _e-mail_, pois o envio de _e-mail_ é feito a partir de relação de confiança entre o servidor de aplicação e o servidor de _e-mail_
    -->

    - **Conta de *e-mail*:**

<!--
    Usuário de e-mail do sistema utilizado para enviar _e-mail_
-->

<!--
    Bloco 5
-->

  - ***Storage*:**
    
      - **Diretório:**

<!--
    Informar apenas quando a utilização for do tipo arquivo (_filesystem_)
-->

### 6.2 Cliente

<!--
    Colocar as configurações do ambiente cliente, quando aplicável
-->

Se a tecnologia utilizada for **JAVA**:

  - **Tipo de Cliente:**

<!--
    Remova os colchetes da alternativa correspondente
    Interno ao TSE
    Interno ao TSE e TREs
    Interno ao TSE
    Público em geral (Internet)
-->

  - **Realiza armazenamento local?**

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Sim, diretório: indicar diretório
    Não se aplica, até então.
-->

  - **Utiliza o SIS?**

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Sim
    Não
-->

## 7. Visão de Disponibilidade

  - **Informações sobre quantitativos e estimativas:**
    
    - **Qtd. estimada de usuários (não simultâneos):**

    <!--
    Remova os colchetes da alternativa correspondente
    Até 500
    De 500 até 1000
    De 1000 até 5000
    Acima de 5000
    -->

    - **Qtd. estimada de usuários simultâneos:**

    <!--
    Remova os colchetes da alternativa correspondente
    De 1 até 50
    De 50 até 200
    De 200 até 500
    Acima de 500
    -->

    - **Qtd. transações / período:**

    <!--
    Estimativa
    -->

    - ***Bytes* / transação de rede:**

    <!--
    Estimativa
    -->

    - **Requisições / segundo:**

<!--
    Estimativa
-->

  - **Sazonalidade:** (períodos de pico de utilização)

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Ao final do mês(es): xxxxx
    Ao início do mês(es): xxxxx
    Todo o dia da semana: xxxxx
    Não se aplica
    Outro, especificar: xxxxx
-->

  - **Utiliza ou não a contratação de serviço de distribuição de conteúdo – CDN?**

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Sim, especifique: xxxxx
    Não se aplica
-->

  - **Disponibilidade do sistema:** (dias da semana)

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Somente em dias úteis
    Todos os dias (inclusive sábados, domingos e feriados)
    Somente ____ dias ANTES de Eleição
    Somente DURANTE o dia da Eleição
    Somente ____ dias APÓS a Eleição
    Outra, especificar: xxxxx
-->

  - **Disponibilidade do sistema:** (horários)

<!--
    De ___:___ às ___:___ horas
-->

  - **Escalabilidade:**

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Aplicação preparada para balanceamento de carga F5 BigIP
    Aplicação preparada para ambiente distribuído
    Aplicação preparada para rodar em cluster
    Aplicação utiliza mesmo servidor para SGBD e App Srv
    Outra, especificar: xxxxx
-->

  - **Sessões de usuário:** (utilização)

<!--
    Remova os colchetes da alternativa correspondente
    Manter informações na sessão do usuário
    Não manter informações em sessão
-->

## 8. Visão de Integração

  - **Interfaces com outros sistemas:** (sejam internos ou externos à Justiça Eleitoral)

<!--
    Remova os colchetes da alternativa correspondente
    Sim, conforme modo de integração por sistema indicadas a seguir
    Não
-->

<!--
    Atenção:
    Caso haja interface com outros sistemas, identificá-los e informar o modo de integração considerando as questões abaixo
    Preencher um bloco de informações para cada sistema (Sistema1, Sistema2...)
    Não havendo interface com outros sistemas, excluir os blocos abaixo
-->

  - **Identificação do sistema em interface**: **Nome do sistema1**

  - **Forma de integração:**
    
    - **Tipo:**

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    _Webservice_
    Arquivo (FTP)
    EJB
    DB _Link_
    Banco de Dados (mesmo banco)]
    RMI
    Outro, especifique: xxxxx
-->

- **Configurações:**
    
    - **URL:**

    <!--
    Remova os colchetes da(s) alternativa(s) correspondente(s) e complemente a informação quando necessário
    Produção: Nome do servidor e porta
    Desenvolvimento: Nome do servidor e porta
    Homologação: Nome do servidor e porta
    -->

    - **Servidor:**

    <!--
    Remova os colchetes da(s) alternativa(s) correspondente(s) e complemente a informação quando necessário
    Produção: Nome do servidor e porta
    Desenvolvimento: Nome do servidor e porta
    Homologação: Nome do servidor e porta
    -->

    - **Protocolo:**

    <!--
    Incluir protocolo
    -->

    - **Porta:**

    <!--
    Informar porta
    -->

    - **Outros:**

    <!--
    Texto livre
    -->

    - **Esquema:**

    <!--
    Especificar esquema
    -->

    - **Banco de Dados:**

    <!--
    Banco de dados
    -->

## 9. Visão de Armazenamento de Dados

  - **Detalhes do modelo de dados:**

<!--
    Remova os colchetes da alternativa correspondente
    Não se aplica até então
    Conforme especificado abaixo:
-->

<!--
    Caso não se aplique, exclua o bloco de informações a seguir:
-->

  - **Qtd. tabelas:**

<!-- 
    99999
-->

  - **Qtd. inicial de registros:**

<!--
    99999
-->

  - *Tamanho da base de dados:* (em MB)

<!--
    99999
-->

  - **Taxa de crescimento do BD/período:**

    <!--
    99999
    -->

    - **Estimativa do espaço em disco a ser consumido:** (considerando o banco de dados)

    <!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Até 1 Gb
    De 1 a 10 Gb
    De 10 a 25 Gb
    De 25 a 50 Gb
    De 50 a 100 Gb
    De 100 a 1 Tb
    Mais de 1 Tb
    -->

    - **Estimativa do espaço em disco a ser consumido:** (considerando arquivos armazenados fora do banco de dados, incluindo o servidor de aplicação e *storage*)

    <!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Até 1 Gb
    De 1 a 50 Gb
    De 50 a 100 Gb
    De 100 a 1 Tb
    Mais de 1 Tb
    Não se aplica, até então.
    -->

    - **Serão armazenados os seguintes tipos de dados no banco de dados:**

    <!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Texto
    Imagens
    Vídeo/Áudio
    Arquivos (doc, pdf, xls)
    Não se aplica, até então
    Outros: xxxxxxx
    -->

    - **Serão armazenados os seguintes tipos de dados em FileSystem do servidor de *storage*:**

    <!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Texto
    Imagens
    Vídeo/Áudio
    Arquivos (doc, pdf, xls)
    Não se aplica, até então.
    Outros: xxxxxxx
    -->

    - **Prazo que os dados deverão ser mantidos no FileSystem:**

    <!--
    Remova os colchetes da alternativa correspondente e complemente a informação quando necessário
    Apenas o movimento do dia
    Apenas o movimento da semana
    Apenas o movimento do mês
    Apenas o movimento do ano
    Apenas o movimento dos últimos 5 anos
    Por período indeterminado (sempre)
    Outro (especificar em meses ou anos): xxxxxxx
    Não se aplica, até então.
    -->

## 10. Validações do Documento

<!--
    Mantenha aqui o registro de todas as validações, considerações e solicitações por parte do Núcleo FASI e das seções da Coinf ou de outras áreas envolvidas na avaliação deste documento
-->

### 10.1 Considerações do FASI:

  - **Resultado da Verificação – Arquitetura do *Software*:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação se necessário
    OK - Totalmente aderente aos padrões corporativos da STI
    OK - Aplicação legada em padrões antigos
    OK - Menciona tecnologia fora do escopo deste núcleo no momento, porém necessária
    INDEFERIDO - Informações insuficientes para verificação
    INDEFERIDO - Menciona tecnologia não suportada e sem previsão de suporte
    Outras: xxxxx
-->

  - **Observações do Núcleo FASI:** [*Texto livre*]

  - **Autor do Parecer / Data:** [*Nome do autor do parecer / dd/mm/aaaa*]

### 10.2 Considerações da Coinf/Serede:

  - **Resultado da Verificação - Coinf/Serede:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação se necessário
    OK - Plenamente suportável imediatamente
    OK - Será suportado com simples providências
    OK - Suportável com providências complexas a serem tomadas
    INDEFERIDO – Informações insuficientes para verificação
    INDEFERIDA – Não suportada
    Outras: xxxxx
-->

  - **Observações da Coinf/Serede:** | [*Texto livre*]

  - **Autor do Parecer / Data:** [*Nome do autor do parecer / dd/mm/aaaa*]

### 10.3 Considerações da Coinf/SEBD:

  - **Resultado da Verificação - Coinf/SEBD:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação se necessário
    OK - Plenamente suportável imediatamente
    OK - Será suportada com simples providências
    OK - Suportável com providências complexas a serem tomadas
    INDEFERIDO - Informações insuficientes para verificação
    INDEFERIDA - Não suportada
    Outras: xxxxx
-->

  - **Observações da Coinf/SEBD:** | [*Texto livre*]

  - **Autor do Parecer / Data:** [*Nome do autor do parecer / dd/mm/aaaa*]

  
### 10.4 Considerações da Coinf/Sesop:

  - **Resultado da Verificação - Coinf/Sesop:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação se necessário
    OK - Plenamente suportável imediatamente
    OK - Será suportada com simples providências
    OK - Suportável com providências complexas a serem tomadas
    INDEFERIDO - Informações insuficientes para verificação
    INDEFERIDA - Não suportada
    Outras: xxxxx
-->

  - **Observações da Coinf/Sesop:** | [*Texto livre*]

  - **Autor do Parecer / Data:** [*Nome do autor do parecer / dd/mm/aaaa*]

### 10.5 Considerações da Coinf/SEPD:

  - **Resultado da Verificação - Coinf/SEPD:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação se necessário
    OK - Plenamente suportável imediatamente
    OK - Será suportada com simples providências
    OK - Suportável com providências complexas a serem tomadas
    INDEFERIDO - Informações insuficientes para verificação
    INDEFERIDA - Não suportada
    Outras: xxxxx
-->

  - **Observações da Coinf/SEPD:** | [*Texto livre*]

  - **Autor do Parecer / Data:** [*Nome do autor do parecer / dd/mm/aaaa*]

### 10.6 Considerações da Coinf/Seau:

  - **Resultado da Verificação - Coinf/Seau:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação se necessário
    OK - Plenamente suportável imediatamente
    OK - Será suportada com simples providências
    OK - Suportável com providências complexas a serem tomadas
    INDEFERIDO - Informações insuficientes para verificação
    INDEFERIDA - Não suportada
    Outras: xxxxx
-->

  - **Observações da Coinf/Seau:** | [*Texto livre*]

  - **Autor do Parecer / Data:** [*Nome do autor do parecer / dd/mm/aaaa*]

## 11. Referências

<!--
    Incluir a identificação de todos os documentos referenciados no presente documento e aqueles que fornecem informações suplementares importantes para a análise da solicitação
-->

> SEPROP \<<seprop@tse.jus.br>\> Sobre o *template* Markdown - DAS: versão 1.0 / 02.03.2018 Url do *template* de referência: <http://sticonhecimento.tse.jus.br/cogti/seprop/templates/modelos-de-documentos-do-produs>
