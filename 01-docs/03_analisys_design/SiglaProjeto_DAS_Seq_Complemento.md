![float](../../01_docs/00_imagem_template/Produs_git.png)

## PRODUS - Processo de Desenvolvimento Unificado de *Software*
### DAS - Documento de Arquitetura de *Software*

<br/>

<!--
    Nota 1: Este modelo foi criado para auxiliar na documentação da arquitetura a ser adotada nos sistemas de _software_.
    Nota 2: Se achar conveniente, acrescente um diagrama UML para ilustrar melhor o cenário de algum tópico deste documento.
-->

## 1. Introdução

### 1.1 Finalidade

Este documento apresenta uma visão detalhada da arquitetura do sistema sob diferentes visões. A intenção é documentar  
todas as características arquiteturais importantes adotadas para o projeto.


### 1.2 Breve Descrição do Projeto de *Software*

<!--
    Descreva em linhas gerais o projeto de software
-->


## 2. Classificação do Sistema/Aplicação

<!--
    A descrição apresenta, resumidamente, a finalidade do caso de uso. Um único parágrafo será suficiente para esta descrição.
    Após, indique os itens de cada tópico abaixo, removendo os colchetes da alternativa correspondente:
-->

  - **Documento referente a qual fase de arquitetura:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Arquitetura candidata
    Arquitetura definitiva
    Documentação reversa
-->

  - **Área de negócio:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Judiciária
    Administrativo
    Eleitoral
    Corporativa
-->

  - **Forma de construção da aplicação/sistema:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Implementada 100% no TSE
    Implementada em parceria com algum(ns) TRE(s)
    Implementada em fábrica de _software_
    Solução pacote de _software_ adquirido
    Solução open-source e gratuita
    Outras: _descrever outra forma de construção de aplicação/sistema_
-->

  - **Situação da aplicação/sistema:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Totalmente nova – 1ª versão
    Modernização de versão existente a ser descontinuada
    Melhorias em aplicação legada que continuará funcionando
    Apenas documentação de versão existente que ainda funciona
-->

  - **Aderência aos padrões corporativos de desenvolvimento de *software* do TSE:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Segue totalmente o PRODUS
    Segue parcialmente o PRODUS
    Segue totalmente o MAgil
    Segue parcialmente MAgil
    Não segue e usa metodologia própria
-->

  - **Uso de recursos de infraestrutura e aderência aos padrões corporativos:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Utilizará a infraestrutura e administração padrão e oficial do TSE
    Necessitará de infraestrutura nos TREs administrada pelo TSE
    Necessitará de infraestrutura específica administrada pelo TSE
    Necessitará de infraestrutura totalmente separada
-->

  - **Abrangência de uso do sistema/aplicação:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    TSE
    TRE
    Zona eleitoral
    Partidos políticos
    Qualquer cidadão
    Outro: [_descrever outra forma de abrangência de uso do sistema/plicação_
-->

  - **Plataforma(s) arquitetural(is) a se basear:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Desktop stand-alone
    Desktop cliente-servidor
    Web
    Embarcado na urna eletrônica
    Embarcado em dispositivo móvel
-->


## 3. Visão Funcional

<!--
    Insira diagrama de caso de uso do sistema que apresente o subconjunto dos considerados arquiteturalmente significativos. Eles representam as funcionalidades centrais significativas do sistema final ou que têm uma ampla cobertura arquitetural, ou seja, experimentam muitos elementos arquiteturais ou se enfatizam ou ilustram um ponto frágil específico da arquitetura.
-->

![float](Imagens/DAS_visao-funcional.jpg)


<!--
    Liste as funcionalidades da aplicação que já foram identificadas. Para cada uma delas, diga o seu nome e se é considerada crítica arquiteturalmente (não precisa indicar se é crítica funcionalmente). Elas poderão ser listadas em uma tabela como mostrado abaixo:
    Para o correto preenchimento das colunas 'Crítico Arquiteturalmente' e 'Processamento', deixe apenas o elemento correspondente dentre as opções indicadas nos colchetes
-->

<!--
    Tabela
-->

| **Funcionalidade**    | **Crítico Arquiteturalmente?** | **Processamento**                  | **Iteração**    |
| --------------------- | ------------------------------ | ---------------------------------- | --------------- |
| [*Funcionalidade1*]   | [Sim ou Não]                   | [Assíncrono , Batch ou *Online*]   | [*Iteração1*]   |
| [*Funcionalidade2*]   | [Sim ou Não]                   | [Assíncrono , Batch ou *Online*]   | [*Iteração2*]   |
| [*Funcionalidade3*]   | [Sim ou Não]                   | [Assíncrono , Batch ou *Online*]   | [*Iteração3*]   |
| [*Funcionalidaden*]   | [Sim ou Não]                   | [Assíncrono , Batch ou *Online*]   | [*Iteraçãon*]   |

  
### 3.1 Diagrama de Visão de Alto Nível da Solução

<!--
    Inserir diagrama com o desenho em alto nível da solução que será adotada.
-->

![float](Imagens/DAS_diagrama-visao-alto-nivel.jpg)

<!--
    Figura 1 – Desenho da Solução
-->

## 4. Visão Tecnológica

<!--
    Caso haja diferenças arquiteturais e tecnológicas entre os módulos/subsistemas, para cada um deles preencha o quadro abaixo.
    
    Após, indique os itens de cada tópico abaixo, removendo os colchetes da alternativa correspondente:
-->

  - **Módulo / Subsistema:** **Nome do módulo ou subsistema**

  - **Baseado no(s) Seguinte(s) Documento(s) de Arquitetura de Referência Padrão do TSE:** (Os documentos podem ser encontrados em Documentos de Arquitetura de Referência)

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Arq.Ref. #01.00.00 – Urna Eletrônica
    Arq.Ref. #02.01.00 – Desktop – Delphi/Win32
    Arq.Ref. #02.02.00 – Desktop – Java – Swing/SWT
    Arq.Ref. #03.01.00 – Web – Java – Geral
    Arq.Ref. #03.03.00 – Web – Java – Spring
    Arq.Ref. #03.04.00 – Web – Zope/Plone
    Sistema Legado
-->

  - **Linguagem(ns) /Scripts de Programação Utilizados:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Delphi – Versão: [xxxxx
    Java – Versão: [xxxxx
    Python - Versão: [xxxxx
    C/C++ – Versão: [xxxxx
    Outras: xxxxx
-->

  - **Servidor de Aplicação Alvo:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    JBoss AS – Versão [xxxxx
    Não se aplica
-->

  - **Banco(s) de Dados:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Oracle 11g
    PostgreSQL – Versão: [xxxxx
    Microsoft SQL Server – Versão: [xxxxx
    MySQL – Versão: xxxxx
    Outro: xxxxx
-->

## 5. Visão de Integração

  - **Módulo / Subsistema:** **Nome do módulo ou subsistema**

  - **Haverá interfaces com outros sistemas (sejam internos ou externos à Justiça Eleitoral)?**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Sim
    Não
-->


<!--
    Colocar diagrama de componente, quando for o caso, exemplificando as integrações do sistema em questão com outros sistemas consumindo informações. Representar, no diagrama, a interface do sistema em questão, que fornece informações para outros sistemas.
    Especifique abaixo, para cada sistema que possui interface, o seu modo de integração e o motivo.
-->

<!-- Tabela -->

| **Sistema**    | **Forma de Integração**                                                                                                                                                                                                                                                                                                                    | **Motivo da Integração**                   |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------ |
| [**SPCE**]     | [*Exemplo: Arquivos sequenciais, Arquivos/estruturas XML, Via Stored Procedures compartilhadas, Via Chamadas RPC, Via Broker (CORBA), Via Mensagens Assíncronas (MQ), Via Chamadas RMI/IIOP, Via SOAP Webservices, Via REST Webservices, Via streams binárias sobre HTTP, Via Sockets TCP/IP, Via Protocolo(s) Proprietário(s), Outros*\] | \[*Buscar informações de pessoa física.*\] |
| **Sistema2**   | FI2                                                                                                                                                                                                                                                                                                                                        | MI2                                        |
| **Sistema3**   | FI3                                                                                                                                                                                                                                                                                                                                        | MI3                                        |
| **Sistema4**   | FI4                                                                                                                                                                                                                                                                                                                                        | MI4                                        |
| **Sistema5**   | FI5                                                                                                                                                                                                                                                                                                                                        | MI5                                        |
| **Sistema6**   | FI6                                                                                                                                                                                                                                                                                                                                        | MI6                                        |
| **Sistema7**   | FI7                                                                                                                                                                                                                                                                                                                                        | MI7                                        |

  
## 6. Visão Lógica (Opcional)

<!--
    Descreva as partes significativas do ponto de vista da arquitetura do modelo de projeto (design), bem como sua divisão em subsistemas e pacotes. Além disso, para cada pacote significativo, ela mostra sua divisão em classes e utilitários de classe. Você deve apresentar as classes significativas do ponto de vista da arquitetura e descrever suas responsabilidades, bem como alguns relacionamentos, operações e atributos de grande importância.
-->

## 7. Visão de Implementação

<!--
    Esta sessão traz, de forma detalhada, como será realizada a construção do sistema do ponto de vista da codificação.
-->

![float](Imagens/DAS_visao-implementacao.jpg)

<!--
    Figura 2 – Visão geral das camadas
-->

### 7.1 Camada (Exemplo 1)

<!--
    Incluir digrama de classe com uma visão lógica, o relacionamento entre as classes que compõem essa camada (quando houver).
-->

#### 7.1.1 Componentes e *Frameworks* Utilizados

<!--
    Listar e justificar componentes e frameworks utilizados na camada que NÃO estiverem previstos nos documentos de arquitetura de referência.
-->

<!-- Tabela -->

| ***Framework***  | **Versão**    | **Justificativa**    |
| ---------------- | ------------- | -------------------- |
| [*Framework1*]   | [*Versao1*]   | [*Justificativa1*]   |
| [*Framework2*]   | [*Versao2*]   | [*Justificativa2*]   |
| [*Framework3*]   | [*Versao3*]   | [*Justificativa3*]   |
| [*Framework4*]   | [*Versao4*]   | [*Justificativa4*]   |
| [*Framework5*]   | [*Versao5*]   | [*Justificativa5*]   |
| [*Framework6*]   | [*Versao6*]   | [*Justificativa6*]   |
| [*Framework7*]   | [*Versao7*]   | [*Justificativa7*]   |

#### 7.1.2 Descrição da Camada

<!--
    Explicar detalhadamente a camada do ponto de vista da implementação em si.
    Incluir detalhes de implementação e configuração que precisam ser seguidos no momento da construção.
-->

## 8\. Visão de Processos (Opcional)

<!--
    Esta seção descreve a decomposição do sistema em processos leves (_threads_ simples de controle) e processos pesados (agrupamentos de processos leves). Organize a seção em grupos de processos que se comunicam ou interagem. Descreva os modos principais de comunicação entre processos, como transmissão de mensagens e interrupções.
-->

## 9\. Visão de Implantação (Opcional)

<!--
    Esta seção descreve uma ou mais configurações da rede física (_hardware_), na qual o software é implantado e executado. Ela é uma visão do modelo de implantação. Para cada configuração, ela deve indicar, no mínimo, os nós físicos (computadores, CPUs) que executam o software e as respectivas interconexões (barramento, LAN, ponto a ponto e assim por diante). Inclua também um mapeamento dos processos da visão de processos nos nós físicos.
-->

<!--
    Apresentar diagrama de implantação com todos os pacotes em que serão feitos _deploy_ no sistema.
    O diagrama abaixo é um exemplo:
-->

![float](Imagens/DAS_visao-implantacao.jpg)

<!--
    Figura 3 - Diagrama de Implantação
-->

## 10. Visão de Dados (Opcional)

<!--
    Uma descrição da perspectiva de armazenamento de dados persistentes do sistema. Esta seção será opcional se os dados persistentes forem poucos ou inexistentes ou se a conversão entre o modelo de projeto e o modelo de dados for simples.
-->

## 11. Visão de Segurança

### 11.1 Diretrizes

<!--
    Indique os itens de cada tópico abaixo, removendo os colchetes da alternativa correspondente:
-->

  - **Módulo / Subsistema:** **Nome do módulo ou subsistema**

  - **Terá controle de autenticação de usuários?**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Sim
    Não
    Desconhecido
-->

  - **Terá controle de autorização de usuários para acessar recursos do sistema?**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Sim
    Não
    Desconhecido
-->

  - **Localização do repositório de informações para autenticação:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Em base de dados própria no esquema da aplicação
    Em base de dados corporativa do TSE
    Em servidor LDAP corporativo (MS-AD)
    Via componente corporativo
    Não se aplica
-->

  - **Localização do repositório de informações para autorização:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    Em base de dados própria no esquema da aplicação
    Em base de dados corporativa do TSE
    Em servidor LDAP corporativo (MS-AD)
    Via componente corporativo
    Não se aplica
-->

  - **Forma de implementação do controle de autenticação e autorização:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente
    TSE – ACESSO (Delphi – _Desktop_)
    TSE – ACESSO-_Web_
    TSE – ODIN
    TSE – Zeus
    TSE – SIS
    Com base no _framework Spring Security_
    Solução própria do sistema
    Com base em API do S.O.
    Com base em componente próprio do servidor de aplicação
    Com base em solução proprietária /solução de IAM
    Nome da solução: [xxxxx
    Não se aplica
-->

### 11.2 Informações complementares

<!--
    Colocar nesta sessão qualquer particularidade da segurança aplicada no sistema e no caso de solução de controle de acesso implementada e mantida no próprio sistema. Explicar motivos e detalhar o funcionamento e forma de aplicá-la.
-->

## 12. Visão de Auditoria

<!--
    Definir se existirá auditoria e como será.
-->


### 12.1 De Dados

<!--
    Definir se existirá auditoria no nível de dados.
-->


### 12.2 De Funcionalidade

<!--
    Definir se existirá auditoria de funcionalidade.
-->


## 13. Padrões de Análise e Projeto

<!--
    Listar, definir e justificar todos os padrões usados que não estejam na arquitetura de referência.
-->


## 14. Validações do Documento

<!--
    Mantenha aqui o registro de todas as validações, considerações e solicitações por parte do Núcleo FASI e das seções da Coinf ou de outras áreas envolvidas na avaliação deste documento.
-->


### 14.1 Considerações do FASI:

  - **Resultado da Verificação – Arquitetura do *Software*:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação se necessário
    OK - Totalmente aderente aos padrões corporativos da STI
    OK - Aplicação legada em padrões antigos
    OK - Menciona tecnologia fora do escopo deste núcleo no momento, porém necessária
    INDEFERIDO - Informações insuficientes para verificação
    INDEFERIDO - Menciona tecnologia não suportada e sem previsão de suporte
    Outras: [xxxxx
-->

  - **Observações do Núcleo FASI:** [*Texto livre*]

  - **Autor do Parecer / Data:** [*Nome do autor do parecer / dd/mm/aaaa*]

### 14.2 Considerações da Coinf/Serede:

  - **Resultado da Verificação - Coinf/Serede:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação se necessário
    OK - Plenamente suportável imediatamente
    OK - Será suportado com simples providências
    OK - Suportável com providências complexas a serem tomadas
    INDEFERIDO – Informações insuficientes para verificação
    INDEFERIDA – Não suportada
    Outras: [xxxxx
-->

  - **Observações da Coinf/Serede:** | [*Texto livre*]

  - **Autor do Parecer / Data:** [*Nome do autor do parecer / dd/mm/aaaa*]

### 14.3 Considerações da Coinf/SEBD:

  - **Resultado da Verificação - Coinf/SEBD:**

<!-- end list -->

<!--
    Remova os colchetes da alternativa correspondente e complemente a informação se necessário
    OK - Plenamente suportável imediatamente
    OK - Será suportada com simples providências
    OK - Suportável com providências complexas a serem tomadas
    INDEFERIDO - Informações insuficientes para verificação
    INDEFERIDA - Não suportada
    Outras: xxxxx
-->

  - **Observações da Coinf/SEBD:** | [*Texto livre*]

  - **Autor do Parecer / Data:** [*Nome do autor do parecer / dd/mm/aaaa*]

## 15. Referências

<!--
    Incluir a identificação de todos os documentos referenciados no presente documento e aqueles que fornecem informações suplementares importantes para a análise da solicitação
-->

> SEPROP \<<seprop@tse.jus.br>\> Sobre o *template* Markdown - DAS: versão 4.0.3/ 27.05.2015 Url do *template* de referência: <http://sticonhecimento.tse.jus.br/cogti/seprop/templates/modelos-de-documentos-do-produs>
