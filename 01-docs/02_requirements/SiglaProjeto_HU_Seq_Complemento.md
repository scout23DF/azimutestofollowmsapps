![float](../../01_docs/00_imagem_template/MAgil_git.png)

## Método de Desenvolvimento com Práticas Ágeis
### HU - História de Usuário

<br/>  

## 1. Identificação da história

  - **Número:** [01]

  - **Nome:** [Exemplo: Incluir o agente responsável]

  - **Épico:** [Número do épico ou N/A]

  - **Tema relacionado:** [Exemplo: Incluir o agente responsável]

  - **Descrição:** [Uma história é uma simples afirmação sobre o que o usuário quer fazer com uma característica de software. Ela deve ser escrita de forma simples e concisa, concentrando-se em quem, no que e no porquê de um recurso, e não no como. Estrutura: Como [usuário/papel], quero [meta], para que eu possa [motivo]. Exemplo: COMO um gestor de contas, QUERO incluir os dados básicos sobre os agentes responsáveis dos acertos de conta, PARA manter as informações de agentes atualizadas.]
  

## 2. Critérios de aceitação

<!--
    Os critérios de aceite são pontos como: o que deve ser feito, o que será testado e quais os resultados esperados.
-->


## 3. Especificação de tela

<!--
    Neste tópico, deve-se inserir um protótipo de tela da história, especificar cada um dos campos dos formulários que compõem a tela e definir as regras de apresentação, se houver.
-->


### 3.1. Protótipo da tela

<!--
    Para criação de protótipos, utilize uma ferramenta livre. Sugestões: Mock up pencil/Visio/Mocjuo. Salve a imagem do protótipo de tela na pasta da HU em questão e depois a insira aqui.
-->

![float](Imagens/HU_001/tela_abc.png)


### 3.2. Campos da tela

<!--
    Visa detalhar os campos apresentadas na tela do item 3.1
    
    Orientações para correto preenchimento dos campos da 'Tabela 1'
    Nome do campo: Inserir o nome do campo, conforme tela apresentada no item 3.1.
    Descrição do campo: Descrever o conteúdo que deve ser inserido no campo.
    Tipo de dado: Incluir na célula uma das seguintes opções: Numérico; Alfanumérico; Data; Botão de Ação ou Outro (nesta opção é necessária a documentação de alguma regra vinculada.
    Tam. (Tamanho): Tamanho do campo, no formato NN.
    Obrigatório: Preencher com Sim; Não ou Condicional. Caso o valor para o campo seja “Sim” em algumas situações e “Não” em outras, deverá ser informado o valor “Condicional” e tais condições devem estar claramente especificadas em alguma regra vinculada ao campo.
    Máscara: Trata-se da formatação (máscara) do campo, por exemplo: campos do tipo data podem ser formatados como “DD/MM/AAAA”, “DD/MM” ou “DD.MM.AA”, etc.
    Regra vinculada: Código da(s) regra(s) vinculada(s) ao campo, seja de apresentação ou de negócio: 1. a especificação e o detalhamento das regras de apresentação aplicadas ao caso de uso são registrados em uma seção própria neste documento; e 2. a especificação e o detalhamento das regras do negócio aplicadas ao sistema são registrados em um documento próprio.
    Observações: Observações gerais do campo.
    PE (Processo Elementar): Inserir um 'X' quando da existência de Processo Elementar relacionado ao campo. Para saber mais sobre PE acesse Para saber mais sobre Processo Elementar, acesse a página do guia no Canal do Conhecimento: http://sticonhecimento.tse.jus.br/cogti/sescon/metodologias-processo-e-procedimentos/processo-elementar
-->

**Tabela 1**

| **Nome do Campo** | **Descrição do Campo** | **Tipo de dado** | **Tam.** | **Obrigatório** | **Máscara** | **Regra vinculada**           | **Observações** | **PE** |
| ----------------- | ---------------------- | ---------------- | -------- | --------------- | ----------- | ----------------------------- | --------------- | ------ |
| NC1               | DC1                    | TD1              | Tam1     | Ob1             | Mas1        | [RV01](SiglaProjeto_RMG.adoc) | Obs1            | PE1    |
| NC2               | DC2                    | TD2              | Tam2     | Ob2             | Mas2        | RV2                           | Obs2            | PE2    |
| NC3               | DC3                    | TD3              | Tam3     | Ob3             | Mas3        | RV3                           | Obs3            | PE3    |
| NC4               | DC4                    | TD4              | Tam4     | Ob4             | Mas4        | RV4                           | Obs4            | PE4    |
| NC5               | DC5                    | TD5              | Tam5     | Ob5             | Mas5        | RV5                           | Obs5            | PE5    |
| NC6               | DC6                    | TD6              | Tam6     | Ob6             | Mas6        | RV6                           | Obs6            | PE6    |
| NC7               | DC7                    | TD7              | Tam7     | Ob7             | Mas7        | RV7                           | Obs7            | PE7    |

### 3.3. Regras de apresentação (RAP)

**Tabela 2**

| **ID** | **Regra** | **Descrição da regra** | **Itens relacionados** |
| ------ | --------- | ---------------------- | ---------------------- |
| RAP001 | Regra1    | Desc1                  | IR1                    |
| RAP002 | Regra2    | Desc2                  | IR2                    |
| RAP003 | Regra3    | Desc3                  | IR3                    |
| RAP004 | Regra4    | Desc4                  | IR4                    |
| RAP005 | Regra5    | Desc5                  | IR5                    |
| RAP006 | Regra6    | Desc6                  | IR6                    |
| RAP007 | Regra7    | Desc7                  | IR7                    |

### 3.4. Informações gerais da tela

<!--
    Neste tópico, poderão ser especificadas informações gerais aplicáveis à tela e que, por algum motivo, não foi possível descrevê-las em outras seções da história de usuário. Poderão constar, por exemplo, informações acerca da forma de acesso, de links existentes na tela, de requisitos especiais ou mais complexos representados por diagramas de visão geral, de atividades, fluxos de processos em BPMN etc
-->


## 4. Referências

<!--
    Essa subseção deve fornecer uma lista completa de todos os documentos referenciados em qualquer ponto da história de usuário. Especificar as fontes em que as referências podem ser obtidas.
-->

  
  

> SEPROP \<<seprop@tse.jus.br>\> Sobre o *template* Markdown - HU: versão 1.0/ 02.03.2018 Url do *template* de referência: <http://sticonhecimento.tse.jus.br/cogti/seprop/templates/modelos-de-documentos-do-magil>
