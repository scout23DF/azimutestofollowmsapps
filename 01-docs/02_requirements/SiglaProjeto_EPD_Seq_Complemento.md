![float](../../01_docs/00_imagem_template/Produs_git.png)

## PRODUS - Processo de Desenvolvimento Unificado de *Software*
### EPD - Especificação de Processamento de Dados

<br/>

<!---
    Passo a passo:
    1. No item “6. Mapeamento de dados”, informar as regras relacionadas ao campo de origem/destino e respectivo ID (REF001) nas colunas “Regras aplicáveis e referência relacionada”;
    2. Para cada regra citada na tabela do item 6, deverão ser informados, no item “7. Identificação das referências utilizadas”, os dados do documento do qual foi extraída a regra apresentada;
    3. Todas as referências inseridas no item 7 deverão ter um INDICADOR criado, que funcionará como vínculo para criação da referência cruzada por meio de link;
    4. Retorne ao item 6 e crie um link em cada ID de referência (REF001), citando o INDICADOR criado da tabela do item 7.
    Obs.: Observar exemplos já disponíveis no template.
    
    Nota: Este modelo pode ser aplicado isoladamente ou de forma complementar ao caso de uso.
-->

## 1. Introdução

A finalidade deste documento é descrever os dados, requisitos e regras aplicados no processamento de dados do(a) [Nome da iniciativa].

  

## 2. Objetivo do processo

<!--
    Descreva, em linhas gerais, o objetivo do processo. A descrição apresenta, resumidamente, a finalidade do processo. Um único parágrafo será suficiente para esta descrição. Ex.: Carga da tabela Fato dos Indicadores.
-->

  

## 3. Descrição do processo

<!--
    Descrição do processamento e visão da perspectiva a ser especificada, ou somente a descrição do processamento quando a visão não for necessária.
    
    Nota:
    Caso o fluxo de processamento contemple somente a extração e carga dos dados, em uma nova estrutura de dados (bancos, arquivos ou estrutura em memória), a descrição do fluxo de processamento é opcional.
    
    Exemplo:
    1. O processo consiste em gerar os dados dos indicadores mensalmente.
    2. Os dados serão gerados utilizando-se as seguintes Flat Files: a) IGB.CRO_CRONOGRAMA_ESFORCO_DTE ou b) IGB.LIM_LIMITES_DTE c. IGB.IND_INDICADORES_DTE o campo ALE_ALERTA deverá ser recalculado quando gerar os dados mensalmente.
    3. Para cada carga da Flat File IGB.CRO_CRONOGRAMA_ESFORCO_DTE (todo dia 1º do mês subsequente), gerar todos os esforços do projeto dos indicadores, gravando na tabela destino DW.IDNTB_IOF_IND_OPERACIONAIS_FATO, conforme o mapeamento dos dados na tabela a seguir.
    4. Utilizar a função de agregação de esforço mensalmente a partir da Flat File IGB.CRO_CRONOGRAMA__ESFORCO_DTE, considerando o procedimento definido no mapeamento dos dados e regras de transformação.
-->

  

## 4. Mapeamento de dados

**Legenda:**

1.  N = Numérico; A = Alfanumérico; D = Data; O = Outro (neste último caso, esse tipo deverá estar documentado em alguma regra);

2.  Caso o valor para o campo seja condicional (isto é, Sim em algumas situações e Não para outras), deverá ser informado o valor “C" = Condicional e tais condições devem estar claramente especificadas em alguma regra.

<!-- end list -->

<!-- Tabela -->

| ORIGEM                                |                                  |                           |                    |                                           |                             |                                                                                                                            |DESTINO                                 |                                   |                           |                     |                                           |                             |                                                                                                                            |
| ------------------------------------- | -------------------------------- | ------------------------- | ------------------ | ----------------------------------------- | --------------------------- | -------------------------------------------------------------------------------------------------------------------------- | -------------------------------------- | --------------------------------- | ------------------------- | ------------------- | ----------------------------------------- | --------------------------- | -------------------------------------------------------------------------------------------------------------------------- |
| **Entidade de ORIGEM**                | **Campo de ORIGEM**              | **Tipo** <sup>**1**</sup> | **Tam.**           | **Valor Padrão**                          | **Obrig.** <sup>**2**</sup> | **Regras aplicáveis e referência relacionada**                                                                             | **Entidade de DESTINO**                | **Campo de DESTINO**              | **Tipo** <sup>**1**</sup> | **Tam.**            | **Valor Padrão**                          | **Obrig.** <sup>**2**</sup> | **Regras aplicáveis e referência relacionada**                                                                             |
| [*Apresentar a entidade de ORIGEM*]   | [*ID-Nome do campo na ORIGEM*]   | [*N,A, D, O*]             | [*Tam1\_ORIGEM*]   | [*Informar o padrão, quando aplicável*]   | [*S,N,C,N/A*]               | [*Referenciar regras relacionadas ao campo de ORIGEM – ID do documento de referência da regra apresentada. Ex.: REF001*]   | [*Apresentar a entidade de DESTINO*]   | [*ID-Nome do campo na DESTINO*]   | [*N,A, D, O*]             | [*Tam1\_DESTINO*]   | [*Informar o padrão, quando aplicável*]   | [*S,N,C,N/A*]               | [*Referenciar regras relacionadas ao campo de origem – ID do documento de referência da regra apresentada. Ex.: REF001*]   |
| Ent.Ori.2                             | Cam.Ori.2                        | Tip.Ori.2                 | Tam.Ori.2          | VP.Ori.2                                  | Ob.Ori.2                    | Reg.Ori.2                                                                                                                  | Ent.Dest.2                             | Cam.Dest.2                        | Tip.Dest.2                | Tam.Dest.2          | VP.Dest.2                                 | Ob.Dest.2                   | Reg.Dest.2                                                                                                                 |
| Ent.Ori.3                             | Cam.Ori.3                        | Tip.Ori.3                 | Tam.Ori.3          | VP.Ori.3                                  | Ob.Ori.3                    | Reg.Ori.3                                                                                                                  | Ent.Dest.3                             | Cam.Dest.3                        | Tip.Dest.3                | Tam.Dest.3          | VP.Dest.3                                 | Ob.Dest.3                   | Reg.Dest.3                                                                                                                 |
| Ent.Ori.4                             | Cam.Ori.4                        | Tip.Ori.4                 | Tam.Ori.4          | VP.Ori.4                                  | Ob.Ori.4                    | Reg.Ori.4                                                                                                                  | Ent.Dest.4                             | Cam.Dest.4                        | Tip.Dest.4                | Tam.Dest.4          | VP.Dest.4                                 | Ob.Dest.4                   | Reg.Dest.4                                                                                                                 |
| Ent.Ori.5                             | Cam.Ori.5                        | Tip.Ori.5                 | Tam.Ori.5          | VP.Ori.5                                  | Ob.Ori.5                    | Reg.Ori.5                                                                                                                  | Ent.Dest.5                             | Cam.Dest.5                        | Tip.Dest.5                | Tam.Dest.5          | VP.Dest.5                                 | Ob.Dest.5                   | Reg.Dest.5                                                                                                                 |
| Ent.Ori.6                             | Cam.Ori.6                        | Tip.Ori.6                 | Tam.Ori.6          | VP.Ori.6                                  | Ob.Ori.6                    | Reg.Ori.6                                                                                                                  | Ent.Dest.6                             | Cam.Dest.6                        | Tip.Dest.6                | Tam.Dest.6          | VP.Dest.6                                 | Ob.Dest.6                   | Reg.Dest.6                                                                                                                 |
| Ent.Ori.7                             | Cam.Ori.7                        | Tip.Ori.7                 | Tam.Ori.7          | VP.Ori.7                                  | Ob.Ori.7                    | Reg.Ori.7                                                                                                                  | Ent.Dest.7                             | Cam.Dest.7                        | Tip.Dest.7                | Tam.Dest.7          | VP.Dest.7                                 | Ob.Dest.7                   | Reg.Dest.7                                                                                                                 |
| Ent.Ori.8                             | Cam.Ori.8                        | Tip.Ori.8                 | Tam.Ori.8          | VP.Ori.8                                  | Ob.Ori.8                    | Reg.Ori.8                                                                                                                  | Ent.Dest.8                             | Cam.Dest.8                        | Tip.Dest.8                | Tam.Dest.8          | VP.Dest.8                                 | Ob.Dest.8                   | Reg.Dest.8                                                                                                                 |

  

## 5. Identificação das referências utilizadas

<!-- Tabela -->

| **ID referência** | **Nome do documento**                             | **Descrição/Localização**                                                         | **Área responsável**             |
| ----------------- | ------------------------------------------------- | --------------------------------------------------------------------------------- | -------------------------------- |
| [*REF001*]        | [*HU\_SPCA\_Incluir Agente Responsavel*]          | [*N/A*]                                                                           | [*Informar área responsável1*]   |
| [*REF002*]        | [*HU001\_SPCA\_Incluir contas bancarias*]         | [*N/A*]                                                                           | [*Informar área responsável2*]   |
| [*REF003*]        | [*CDU\_SPCA\_024\_Manter\_Origem\_de\_Recurso*]   | <http://git.tse.jus.br/eleitoral/spca-cadastro/tree/master/01_docs/03_requisitos> | [*Informar área responsável3*]   |
| [*REF004*]        | [*RN\_SPCA*]                                      | <http://www.tse.jus.br/>                                                          | [*Informar área responsável4*]   |

  
  

> SEPROP \<<seprop@tse.jus.br>\> Sobre o *template* Markdown - EPD: versão 1.0/ 01.03.2018 Url do *template* de referência: <http://sticonhecimento.tse.jus.br/cogti/seprop/templates/modelos-de-documentos-do-produs>
