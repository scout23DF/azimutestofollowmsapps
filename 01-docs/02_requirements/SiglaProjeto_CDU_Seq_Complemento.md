![float](../../01_docs/00_imagem_template/Produs_git.png)

## PRODUS - Processo de Desenvolvimento Unificado de *Software*
### CDU - Especificação de Caso de Uso

<br/>

<!---
    Nota 1: Havendo algum arquivo envolvido no processamento dos fluxos de eventos, utilizar o modelo (template) de “Especificação de Layout de Arquivos” para o seu detalhamento. Esse documento complementará a especificação de caso de uso como anexo e deverá ser citado no item 10 – Referências.
    
    Nota 2: Havendo a necessidade de descrever em detalhes dados, requisitos e regras aplicados no processamento de dados do sistema, o documento “Especificação de Processamento de Dados”, também poderá ser utilizado de forma complementar a este caso de uso.
    
    Nota 3: No 'Commite message', descrever as alterações que resultaram na versão em questão. [Descrição das alterações que resultaram nesta versão.
    Exemplos de inclusões: Inclusão do ator [nome do ator]; nclusão dos fluxos alternativos FA 3, FA 4 e FA 5; Inclusão do fluxo de exceção FE 3; Inclusão da regra de negócio RN016 ao passo 5 do fluxo básico; Inclusão dos campos ‘Eleição Suplementar’ e ‘Cargo’ no leiaute do protótipo PT 1; Inclusão da regra de apresentação RAP004.
    Exemplos de alterações: Alteração aos passos 2 e 3 do fluxo alternativo FA 1;Alteração do tamanho dos campos ‘Eleição Suplementar’ e ‘Cargo’ no leiaute do protótipo PT 1.
    Exemplos de exclusões: Exclusão do requisito especial RE001; Remoção da regra de negócio RN029 no passo 5 do fluxo básico; Exclusão dos campos ‘Ano da Eleição’ e ‘Situação’ do leiate do protótipo PT 2.
-->

## 1. Introdução

A finalidade deste documento é detalhar um caso de uso. Esse detalhamento descreve as instâncias (cenários) de casos de uso, no qual cada instância representa um conjunto de passos que devem ser executados pelo sistema, com o objetivo de produzir algo significativo para o(s) ator(es). Seu escopo abrange a descrição dos atores envolvidos na funcionalidade, a sequência de ações a serem realizadas pelo sistema e pelo ator e uma série de características específicas do caso de uso em questão. Termos e abreviaturas específicos podem ser encontrados no glossário do respectivo sistema.

  

## 2. Descrição breve

<!---
    A descrição apresenta, resumidamente, a finalidade do caso de uso. Um único parágrafo será suficiente para esta descrição.
    
    Esta descrição deve ser a mesma especificada no Modelo UML de Caso de Uso. A descrição do Caso de Uso deverá ser mantida somente neste Modelo de Caso de Uso.
    
    Exemplos: Este caso de uso permite ao cidadão obter relatórios de estatísticas de candidaturas das eleições ordinárias; Este caso de uso permite ao eleitor consultar a situação da inscrição eleitoral (título) que informa a validade do título para o exercício do voto.
-->

Este caso de uso permite [*nome do ator principal*] [*descrever brevemente a finalidade deste caso de uso*].

  

## 3. Ator(es)

<!--
    Liste o nome de todos os atores que interagem com o caso de uso.
    O nome do(s) ator(es) deve ser o mesmo especificado no Modelo de Caso de Uso. A descrição do(s) ator(es) deverá ser mantida somente neste Modelo de Caso de Uso.
-->

Segue descrição do(s) ator(es) envolvido(s) na execução do caso de uso:

  - nome ator 1:

  - nome do ator2:

O **Relatório Sintético do Modelo de Casos de Uso** contém a descrição específica de cada ator.

  

## 4. Pré-condições

<!--
    Liste, quando aplicável, as pré-condições que descrevem o estado em que o sistema deve estar antes do caso de uso ser iniciado.
    
    Quando nenhuma pré-condição for identificada, preencher com o texto “Esta seção não se aplica ao caso de uso em questão”.
-->

<!-- Tabela -->

| **Pré-condição**                                           | **Descrição da Pré-condição**                                                                                                                                          |
| ---------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [*O ator deve estar autenticado no sistema*]               | [*O procedimento de autenticação do ator no ODIN deve ter sido concluído com sucesso e a sessão na aplicação ainda deve estar válida*]                                 |
| [*O ator deve ter permissão de acesso à funcionalidade*]   | [*O perfil associado ao ator no subsistema ACESSO (Sistema de Controle de Acesso) deve ter permissão de execução das funcionalidades vinculadas a este Caso de Uso*]   |
| [*Pré-condição - PC*]                                      | [*Descrição da pré-condição - Desc.*]                                                                                                                                  |
| PC4                                                        | Desc4                                                                                                                                                                  |
| PC5                                                        | Desc5                                                                                                                                                                  |
| PC6                                                        | Desc6                                                                                                                                                                  |
| PC7                                                        | Desc7                                                                                                                                                                  |
| PC8                                                        | Desc8                                                                                                                                                                  |
| PC9                                                        | Desc9                                                                                                                                                                  |
| PC10                                                       | Desc10                                                                                                                                                                 |

  

## 5. Pós-condições

<!--
    Liste, quando aplicável, as pós-condições do caso de uso. Uma pós-condição de um caso de uso é uma lista de estados possíveis em que o sistema pode estar, imediatamente após a conclusão de um Caso de Uso.
-->

<!-- Tabela -->

| **Pós-condição**                                              | **Descrição da Pré-condição**                                                                                                                                                                                              |
| ------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [*Log de dados incluídos, alterados ou excluídos*]            | [*Os dados incluídos, alterados ou excluídos devem ser armazenados em um registro (log) acompanhados de informações sobre o evento (data, hora, tipo e usuário)*]                                                          |
| [*Opção de solicitação de margem consignável indisponível*]   | [*Ao final do caso de uso, a opção de solicitação de margem consignável torna-se indisponível para a execução de outra solicitação enquanto pendente de análise a solicitação anteriormente realizada pelo Caso de Uso*]   |
| [*Pós-condição - PC*]                                         | [*Descrição da pré-condição - Desc.*]                                                                                                                                                                                      |
| PC4                                                           | Desc4                                                                                                                                                                                                                      |
| PC5                                                           | Desc5                                                                                                                                                                                                                      |
| PC6                                                           | Desc6                                                                                                                                                                                                                      |
| PC7                                                           | Desc7                                                                                                                                                                                                                      |
| PC8                                                           | Desc8                                                                                                                                                                                                                      |
| PC9                                                           | Desc9                                                                                                                                                                                                                      |
| PC10                                                          | Desc10                                                                                                                                                                                                                     |

  

## 6. Fluxo de eventos

<!--
    Orientações Importantes:
    
    Os passos dos fluxos devem ser escritos em períodos simples e em ordem direta:
    
    1. Sujeito... verbo... complementos verbais .. [adjunto adnominal/complemento nominal];
    2. O sistema... deduz... a quantidade ... do saldo da conta.
    3. Descreva, nos passos dos fluxos, a intenção do ator, não seus movimentos no uso de uma Interface de Usuário, que consiste em um dos erros mais comuns e graves na escrita de caso de uso.
    4. Todos os identificadores das regras de negócio (RN) deverão estar referenciados no final do(s) passo(s) de seu Fluxo de Eventos de origem: não é suficiente estarem referenciados apenas em outro local do documento (fluxos de exceção, campos e comandos de protótipos de tela, entre outros).
-->

### 6.1. Fluxo Básico (FB): [*Nome do Fluxo*]

<!--
    Descreva os passos do caminho normal do Caso de Uso (conhecido também como “caminho feliz” ou “cenário positivo”), no qual o objetivo desejado pelo ator é concluído com sucesso, sem que existam exceções (erros, entradas inesperadas, etc.) ou caminhos alternativos de execução. O último passo deste fluxo deve deixar claro que o caso de uso é finalizado.
-->

Este caso de uso é iniciado quando o ator [*descrever a ação do ator que inicia as ações deste Caso de Uso*].

1.  O sistema [*apresenta (…) [PT1]*];

2.  O ator [*seleciona (…) [FA1][FA2][PE1]*];

3.  O sistema [*apresenta (…) [PT2]*];

4.  O ator [*informa (…)*];

5.  O sistema verifica [*(…) [FE1] [RN001]*];

6.  Incluir o caso de uso [*nome do caso de uso*] para [*objetivo geral da inclusão realizada*];

7.  O sistema [*apresenta (…)*];

8.  O ator [*solicita (…) [FA3]*];

9.  O sistema [*realiza (…) [FE2] [RN002]*];

10. […];

11. O caso de uso é finalizado.

  

### 6.2. Fluxos Alternativos (FAs)

#### FA 1. [*Nome do Fluxo Alternativo 1*]

<!--
    O último passo deste fluxo deve deixar claro que ou caso de uso é finalizado, ou retorna ao fluxo em que ocorreu o desvio das ações para este fluxo alternativo.
-->

No passo [*número do passo do fluxo básico ou do fluxo alternativo em que existe a possibilidade de ocorrência de um fluxo alternativo*] do [*fluxo básico ou fluxo alternativo*], caso [*descrição da condição de ocorrência do fluxo alternativo*], o sistema deve [*descrição da ação a ser tomada pelo sistema*].

1.  O ator [*informa (…)*];

2.  O sistema [*verifica (…) [FE3] [RN004]*];

3.  O sistema [*apresenta (…)*];

4.  O ator [*solicita (…) [FA4]*];

5.  O sistema [*realiza (…) [FE4] [RN005]*];

6.  O sistema [*apresenta (…)*];

7.  O ator [*confirma (…) [FA5]*];

8.  […];

9.  O sistema retorna ao [*fluxo básico ou fluxo alternativo em que ocorreu o desvio das ações para este em questão*]

  

#### FA 2. [*Nome do Fluxo Alternativo 2*]

No passo [*número do passo do fluxo básico ou do fluxo alternativo em que existe a possibilidade de ocorrência de um fluxo alternativo*] do [*fluxo básico ou fluxo alternativo*], caso [*descrição da condição de ocorrência do fluxo alternativo*], o sistema deve [*descrição da ação a ser tomada pelo sistema*].

1.  O ator [*informa (…)*];

2.  O sistema [*verifica (…)*];

3.  […];

4.  O caso de uso é finalizado.

  

### 6.3. Fluxos de Exceção (FE)

#### FE 1. [*Nome do Fluxo de Exceção 1*]

<!--
    O último passo deste fluxo deve deixar claro que ou o caso de uso é finalizado, ou retorna ao fluxo em que ocorreu a exceção.
-->

No passo [*número do passo do fluxo básico ou do fluxo alternativo em que existe a possibilidade de ocorrência de um fluxo de exceção*] do [*fluxo básico ou fluxo alternativo*], caso o sistema verifique que [*descrição da condição de ocorrência do fluxo de exceção*]:

1.  O sistema [*informa (…)*];

2.  O ator [_confirma (…)-];

3.  [*O sistema retorna ao passo anterior do fluxo em que ocorreu a exceção*]

  

#### FE 2. [*Nome do Fluxo de Exceção 2*]

No(s) passo(s) [*número do(s) passo(s) do fluxo básico ou do fluxo alternativo em que existe a possibilidade de ocorrência de um fluxo de exceção*] do [*fluxo básico ou fluxo alternativo*], caso o sistema verifique que [*descrição da condição de ocorrência do fluxo de exceção*]:

1.  O sistema [*informa (…)*];

2.  O ator [*confirma (…)*];

3.  […];

4.  [*O caso de uso é finalizado.*]

  

## 7. Ponto de Extensão (PE)

<!--
    Indique, quando aplicável, nesta seção, a localização do ponto de extensão no fluxo de eventos. O Caso de Uso de “extensão” descreve um comportamento opcional inserido neste Caso de Uso em tela, considerado o Caso de Uso base.
    
    Exemplo:
    PE1. Manter Dependente: No passo 2 do Fluxo Básico, caso o ator deseje alterar dados de um dependente, o sistema deve estender o caso de uso Manter Dependentes
-->

### PE 1. [*Nome do Ponto de Extensão 1*]

<!--
    Descrição do fluxo de extensão
-->

### PE 2. [*Nome do Ponto de Extensão 2*]

<!--
    Descrição do Fluxo de Extensão
-->

  

## 8. Requisitos especiais

<!--
    Descreva, quando aplicável, qualquer requisito que possa estar relacionado ao caso de uso, mas que não seja considerado no Fluxo de Eventos. Quando nenhum requisito deste tipo for aplicável ao caso de uso, preencher com o texto “Esta seção não se aplica ao caso de uso em questão”.
    
    Um requisito especial é, geralmente, um requisito não funcional e específico de um caso de uso, mas que não se especifica naturalmente no texto do fluxo de eventos do caso de uso. Exemplos de requisitos especiais incluem requisitos legais e regulatórios, padrões de aplicativos e atributos de qualidade do sistema a ser construído, incluindo requisitos de usabilidade, confiabilidade, desempenho, etc.
    
    Quando for necessária a especificação de um requisito especial ser utilizada e/ou referenciada por outros casos de uso, utilize a Especificação Suplementar (template “ESP – Especificação Suplementar”).
-->

<!-- Tabela -->

| **ID**    | **Requisito Especial**                   | **Descrição do Requisito**                                                                                                                                                                                 |
| --------- | ---------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **RE001** | [*Tempo de Execução da Transação*]       | [*O tempo de execução das transações deste caso de uso devem ser inferiores a 30 segundos*]                                                                                                                |
| **RE002** | [*Resolução TSE 23.380 de 08/05/2012*]   | [*Os cálculos do adicional de qualificação deverão considerar as normas expressas na Resolução TSE 23.380, de 08/05/2012, que dispõe sobre o Adicional de Qualificação no âmbito da Justiça Eleitoral*]    |
| **RE003** | [*Nome do Requisito Especial - RE*]      | [*Descrição do requisito especial - Desc*]                                                                                                                                                                 |
| **RE004** | RE4                                      | Desc4                                                                                                                                                                                                      |
| **RE005** | RE5                                      | Desc5                                                                                                                                                                                                      |
| **RE006** | RE6                                      | Desc6                                                                                                                                                                                                      |
| **RE007** | RE7                                      | Desc7                                                                                                                                                                                                      |

  

## 9. Informações Complementares

<!--
    Inclua, se necessário, ou forneça referências a informações adicionais requeridas para explicar o caso de uso e que, por algum motivo, não foi possível descrevê-las em outras seções da especificação de Caso de Uso. Isso pode incluir, por exemplo, diagramas de visão geral, diagramas de atividades, fluxos de processo em BPMN, exemplos ou qualquer outra coisa que você imagine como relevante para a compreensão do Caso de Uso.
    
    Este item, também, deverá relacionar arquivos envolvidos num determinado processamento dos fluxos de eventos, se houver. Para o detalhamento de layout de arquivo, deverá ser utilizado o documento “Especificação de Layout de Arquivos”.
-->

  

## 10. Referências

<!--
    Essa subseção deve fornecer uma lista completa de todos os documentos referenciados em qualquer ponto do caso de uso. Especificar as fontes em que as referências podem ser obtidas.
-->

<br/>  
  

## Anexo I

### Visão Geral dos Casos de Teste

  

## 1. Interfaces gráficas de usuário

A finalidade deste anexo é **descrever os elementos e as regras de apresentação da interface do caso de uso em questão**.

Seu escopo abrange a descrição do caminho e das características e regra(s) de apresentação dos campos dos formulários que deverão compor cada interface gráfica relativa ao caso de uso a que a interface está associada. Caso este caso de uso seja responsável pela emissão de relatórios, a formatação e as informações que compõem os respectivos relatórios também estarão especificadas neste anexo.

  

### 1.1 Forma de acesso

<!--
    Definir a forma de acesso à interface gráfica, por exemplo, caso a forma de acesso à interface seja por meio de menu, indique os itens do menu que deverão ser selecionados. Caso a forma de acesso seja por meio de botão, defina como chegar a este botão. Caso o sistema utilize o ODIN, defina a forma de acesso.
    
    Também é possível que as diferentes permissões do(s) ator(es) envolvido(s) na execução deste caso de uso sejam descritas neste item. Havendo, porém, a necessidade de se especificar em detalhes o relacionamento entre os perfis e as funcionalidades do sistema como um todo, recomenda-se a utilização do documento “Especificação de Perfis de Acesso”.
-->

  

### 1.2 Leiaute

#### PT 1. [*Título da interface gráfica de usuário 1*]

<!--
    Inclua a(s) imagem(ns) do protótipo de interface do caso de uso e especifique, na lista descritiva abaixo, os campos de formulários e os comandos de função.
-->

![float](Imagens/CDU_001/tela_abc.jpg)

  

#### Campos do formulário - PT 1

<!--
    A tabela abaixo define os campos de formulário da interface gráfica. As seguintes propriedades devem ser definidas:
    Nome do campo: Inserir o nome do campo.
    Descrição do campo: Descrever o conteúdo que deve ser inserido no campo.
    Tipo de dado: Incluir na célula uma das seguintes opções: Numérico; Alfanumérico; Data; Botão de Ação ou Outro (nesta opção é necessária a documentação de alguma regra vinculada).
    Tam. (Tamanho): Tamanho do campo, no formato NN.
    Formatação: Formatação (máscara) do campo, por exemplo: campos do tipo data podem ser formatados como “dd/mm/aaaa”, “dd/mm”, “dd.mm.aa”, etc.
    Editável: Editável e obrigatoriedade. Indicar “Sim”, “Não”, “Condicional”. Caso o valor para o campo seja condicional (isto é, Sim em algumas situações e Não em outras), deverá ser informado o valor “Condicional” e tais condições devem estar claramente especificadas em alguma regra vinculada ao campo.
    Único (Unicidade): Campos que não podem ser duplicados no banco de dados (ex.: chaves primárias) – marcar “x” quando a opção for aplicável.
    Domínio: Domínio de valores válidos para o campo, por exemplo, o campo “Forma de Aquisição” pode possuir os seguintes valores: Compra Direta, Doação, Licitação e Outro.
    Valor Padrão: Valor padrão a ser apresentado ou selecionado quando a interface gráfica é apresentada ao usuário.
    Regras vinculadas: Código da(s) regra(s) vinculada(s) ao campo, seja de apresentação ou de negócio: 1. a especificação e o detalhamento das regras de apresentação aplicadas ao caso de uso são registrados em uma seção própria neste documento; e 2. a especificação e o detalhamento das regras do negócio aplicadas ao sistema são registrados em um documento próprio.
-->

**Obs.:**

1.  N = Numérico; A = Alfanumérico; D = Data; O = Outro (neste último caso, esse tipo deverá estar documentado em alguma regra);

2.  Propriedades Específicas: Editável; Obrigatório; Único (Unicidade);

3.  Caso o valor para o campo seja condicional (isto é, Sim em algumas situações e Não para outras), deverá ser informado o valor “Condicional” e tais condições devem estar claramente especificadas em alguma regra.

<!-- end list -->

<!-- Tabela -->

| **Nome**                     | **Descrição**                                                                                                      | **Tipo** <sup>**1**</sup> | **Tam.** | **Formatação**                             | **Editável** <sup>**2**</sup> <sup>**3**</sup> | **Obrig.** <sup>**2**</sup> <sup>**3**</sup> | **Único** <sup>**2**</sup> | **Domínio** | **Valor Padrão** | **Regras vinculadas**                                                       |
| ---------------------------- | ------------------------------------------------------------------------------------------------------------------ | ------------------------- | -------- | ------------------------------------------ | ---------------------------------------------- | -------------------------------------------- | -------------------------- | ----------- | ---------------- | --------------------------------------------------------------------------- |
| [*Título / nome do campo*]   | [*Descrição clara e completa do campo e sua origem: Ex.: “CNPJ do Fornecedor associado ao material informado”*]    | [*N,A, D, O*]             | Tam1     | [*000.000.000-00; dd/mm/aaaa, N/A, etc*]   | [*S, N, C, N/A*]                               | [*S, N, C, N/A*]                             | [*X*]                      | Dom1        | VP1              | [*Quando aplicável, referencie as regras de apresentação ou de negócio*]    |
| N2                           | Desc2                                                                                                              | Tip2                      | Tam2     | For2                                       | Ed2                                            | Ob2                                          | Un2                        | Dom2        | VP2              | RV2                                                                         |
| N3                           | Desc3                                                                                                              | Tip3                      | Tam3     | For3                                       | Ed3                                            | Ob3                                          | Un3                        | Dom3        | VP3              | RV3                                                                         |
| N4                           | Desc4                                                                                                              | Tip4                      | Tam4     | For4                                       | Ed4                                            | Ob4                                          | Un4                        | Dom4        | VP4              | RV4                                                                         |
| N5                           | Desc5                                                                                                              | Tip5                      | Tam5     | For5                                       | Ed5                                            | Ob5                                          | Un5                        | Dom5        | VP5              | RV5                                                                         |
| N6                           | Desc6                                                                                                              | Tip6                      | Tam6     | For6                                       | Ed6                                            | Ob6                                          | Un6                        | Dom6        | VP6              | RV6                                                                         |
| N7                           | Desc7                                                                                                              | Tip7                      | Tam7     | For7                                       | Ed7                                            | Ob7                                          | Un7                        | Dom7        | VP7              | RV7                                                                         |

  

#### Comandos de função - PT 1

<!--
    A tabela abaixo define os comandos da interface gráfica, principalmente os botões. As seguintes propriedades devem ser definidas:
    Imagem: Imagem para o comando, no protótipo de tela.
    Nome: Nome do comando.
    Ação: Ação acionada pelo comando
    Regra de Negócio Relacionada: Regras de apresentação ou de negócio vinculadas ao comando.
-->

<!-- Tabela -->

| **Imagem**                                                                                                                              | **Nome**              | **Ação**                                             | **Regra de Negócio Relacionada**                                                 |
| --------------------------------------------------------------------------------------------------------------------------------------- | --------------------- | ---------------------------------------------------- | -------------------------------------------------------------------------------- |
| [*Insira aqui uma imagem para o comando no protótipo de interface gráfica, por meio da pasta 'Imagens' e subpasta do CDU específico*]   | [*Nome do comando*]   | [*Descrição sucinta da ação executada pelo comando*] | [*Caso aplicável, referencie a regra de negócio relacionada com este comando*]   |
| ![float](Imagens/CDU_001/acao_adicionar_PT1.jpg)                                                                                        | Nom2                  | Aç2                                                  | RN2                                                                              |
| Img3                                                                                                                                    | Nom3                  | Aç3                                                  | RN3                                                                              |
| Img4                                                                                                                                    | Nom4                  | Aç4                                                  | RN4                                                                              |
| Img5                                                                                                                                    | Nom5                  | Aç5                                                  | RN5                                                                              |
| Img6                                                                                                                                    | Nom6                  | Aç6                                                  | RN6                                                                              |
| Img7                                                                                                                                    | Nom7                  | Aç7                                                  | RN7                                                                              |

  

#### *Links* - PT 1

<!--
    Relacionar os links existentes na interface gráfica. Caso não existam links na interface, preencher esta seção com o texto “Este tipo de informação não é aplicável à interface em questão”.
-->

  

#### Regras de Apresentação (RAP) - PT 1

<!-- Tabela -->

| **ID**     | **Regra**                             | **Descrição da regra**                    | **Itens relacionados** |
| ---------- | ------------------------------------- | ----------------------------------------- | ---------------------- |
| **RAP001** | [*Título da regra de apresentação*]   | [*Descrição da regra de apresentação*]    | IR1                    |
| **RAP002** | Reg2                                  | Des2                                      | IR2                    |
| **RAP003** | Reg3                                  | Des3                                      | IR3                    |
| **RAP004** | Reg4                                  | Des4                                      | IR4                    |
| **RAP005** | Reg5                                  | Des5                                      | IR5                    |
| **RAP006** | Reg6                                  | Des6                                      | IR6                    |
| **RAP007** | Reg7                                  | Des7                                      | IR7                    |

  

#### PT 2. [*Título da interface gráfica de usuário 2*]

<!--
    Inclua a(s) imagem(ns) do protótipo de interface do caso de uso e especifique, na lista descritiva abaixo, os campos de formulários e os comandos de função.
-->

![float](Imagens/CDU_001/tela_def.jpg)

  

#### Campos do formulário - PT 2

<!--
    A tabela abaixo define os campos de formulário da interface gráfica. As seguintes propriedades devem ser definidas:
    Nome do campo: Inserir o nome do campo.
    Descrição do campo: Descrever o conteúdo que deve ser inserido no campo.
    Tipo de dado: Incluir na célula uma das seguintes opções: Numérico; Alfanumérico; Data; Botão de Ação ou Outro (nesta opção é necessária a documentação de alguma regra vinculada).
    Tam. (Tamanho): Tamanho do campo, no formato NN.
    Formatação: Formatação (máscara) do campo, por exemplo: campos do tipo data podem ser formatados como “dd/mm/aaaa”, “dd/mm”, “dd.mm.aa”, etc.
    Editável: Editável e obrigatoriedade. Indicar “Sim”, “Não”, “Condicional”. Caso o valor para o campo seja condicional (isto é, Sim em algumas situações e Não em outras), deverá ser informado o valor “Condicional” e tais condições devem estar claramente especificadas em alguma regra vinculada ao campo.
    Único (Unicidade): Campos que não podem ser duplicados no banco de dados (ex.: chaves primárias) – marcar “x” quando a opção for aplicável.
    Domínio: Domínio de valores válidos para o campo, por exemplo, o campo “Forma de Aquisição” pode possuir os seguintes valores: Compra Direta, Doação, Licitação e Outro.
    Valor Padrão: Valor padrão a ser apresentado ou selecionado quando a interface gráfica é apresentada ao usuário.
    Regras vinculadas: Código da(s) regra(s) vinculada(s) ao campo, seja de apresentação ou de negócio: 1. a especificação e o detalhamento das regras de apresentação aplicadas ao caso de uso são registrados em uma seção própria neste documento; e 2. a especificação e o detalhamento das regras do negócio aplicadas ao sistema são registrados em um documento próprio.
-->

**Obs.:**

1.  N = Numérico; A = Alfanumérico; D = Data; O = Outro (neste último caso, esse tipo deverá estar documentado em alguma regra);

2.  Propriedades Específicas: Editável; Obrigatório; Único (Unicidade);

3.  Caso o valor para o campo seja condicional (isto é, Sim em algumas situações e Não para outras), deverá ser informado o valor “Condicional” e tais condições devem estar claramente especificadas em alguma regra.

<!-- end list -->

<!-- Tabela -->

| **Nome**                     | **Descrição**                                                                                                      | **Tipo** <sup>**1**</sup> | **Tam.** | **Formatação**                             | **Editável** <sup>**2**</sup> <sup>**3**</sup> | **Obrig.** <sup>**2**</sup> <sup>**3**</sup> | **Único** <sup>**2**</sup> | **Domínio** | **Valor Padrão** | **Regras vinculadas**                                                       |
| ---------------------------- | ------------------------------------------------------------------------------------------------------------------ | ------------------------- | -------- | ------------------------------------------ | ---------------------------------------------- | -------------------------------------------- | -------------------------- | ----------- | ---------------- | --------------------------------------------------------------------------- |
| [*Título / nome do campo*]   | [*Descrição clara e completa do campo e sua origem: Ex.: “CNPJ do Fornecedor associado ao material informado”*]    | [*N,A, D, O*]             | Tam1     | [*000.000.000-00; dd/mm/aaaa, N/A, etc*]   | [*S,N,C,N/A*]                                  | [*S,N,C,N/A*]                                | [*X*]                      | Dom1        | VP1              | [*Quando aplicável, referencie as regras de apresentação ou de negócio*]    |
| N2                           | Desc2                                                                                                              | Tip2                      | Tam2     | For2                                       | Ed2                                            | Ob2                                          | Un2                        | Dom2        | VP2              | RV2                                                                         |
| N3                           | Desc3                                                                                                              | Tip3                      | Tam3     | For3                                       | Ed3                                            | Ob3                                          | Un3                        | Dom3        | VP3              | RV3                                                                         |
| N4                           | Desc4                                                                                                              | Tip4                      | Tam4     | For4                                       | Ed4                                            | Ob4                                          | Un4                        | Dom4        | VP4              | RV4                                                                         |
| N5                           | Desc5                                                                                                              | Tip5                      | Tam5     | For5                                       | Ed5                                            | Ob5                                          | Un5                        | Dom5        | VP5              | RV5                                                                         |
| N6                           | Desc6                                                                                                              | Tip6                      | Tam6     | For6                                       | Ed6                                            | Ob6                                          | Un6                        | Dom6        | VP6              | RV6                                                                         |
| N7                           | Desc7                                                                                                              | Tip7                      | Tam7     | For7                                       | Ed7                                            | Ob7                                          | Un7                        | Dom7        | VP7              | RV7                                                                         |

  

#### Comandos de função - PT 2

<!--
    A tabela abaixo define os comandos da interface gráfica, principalmente os botões. As seguintes propriedades devem ser definidas:
    Imagem: Imagem para o comando, no protótipo de tela.
    Nome: Nome do comando.
    Ação: Ação acionada pelo comando
    Regra de Negócio Relacionada: Regras de apresentação ou de negócio vinculadas ao comando.
-->

<!-- Tabela -->

| **Imagem**                                                                                                                              | **Nome**              | **Ação**                                             | **Regra de Negócio Relacionada**                                                 |
| --------------------------------------------------------------------------------------------------------------------------------------- | --------------------- | ---------------------------------------------------- | -------------------------------------------------------------------------------- |
| [*Insira aqui uma imagem para o comando no protótipo de interface gráfica, por meio da pasta 'Imagens' e subpasta do CDU específico*]   | [*Nome do comando*]   | [*Descrição sucinta da ação executada pelo comando*] | [*Caso aplicável, referencie a regra de negócio relacionada com este comando*]   |
| ![float](Imagens/CDU_001/acao_salvar_PT2.jpg)                                                                                           | Nom2                  | Aç2                                                  | RN2                                                                              |
| Img3                                                                                                                                    | Nom3                  | Aç3                                                  | RN3                                                                              |
| Img4                                                                                                                                    | Nom4                  | Aç4                                                  | RN4                                                                              |
| Img5                                                                                                                                    | Nom5                  | Aç5                                                  | RN5                                                                              |
| Img6                                                                                                                                    | Nom6                  | Aç6                                                  | RN6                                                                              |
| Img7                                                                                                                                    | Nom7                  | Aç7                                                  | RN7                                                                              |

  

#### *Links* - PT 2

<!--
    Relacionar os links existentes na interface gráfica. Caso não existam links na interface, preencher esta seção com o texto “Este tipo de informação não é aplicável à interface em questão”.
-->

  

#### Regras de Apresentação (RAP) - PT 2

<!-- Tabela -->

| **ID**     | **Regra**                             | **Descrição da regra**                    | **Itens relacionados** |
| ---------- | ------------------------------------- | ----------------------------------------- | ---------------------- |
| **RAP001** | [*Título da regra de apresentação*]   | [*Descrição da regra de apresentação*]    | IR1                    |
| **RAP002** | Reg2                                  | Des2                                      | IR2                    |
| **RAP003** | Reg3                                  | Des3                                      | IR3                    |
| **RAP004** | Reg4                                  | Des4                                      | IR4                    |
| **RAP005** | Reg5                                  | Des5                                      | IR5                    |
| **RAP006** | Reg6                                  | Des6                                      | IR6                    |
| **RAP007** | Reg7                                  | Des7                                      | IR7                    |

  

## 2. Descrição dos relatórios de usuários

<!--
    Descrever o(s) relatório(s) do caso de uso. Caso exista mais de um relatório, repetir todo o item 2.0, por relatório. Caso não existam relatórios no contexto desta interface, os textos em azul desta seção sugerida deverão ser excluídos.
-->

### 2.1 [*Título do relatório 1*]

#### Passos do fluxo de eventos

<!--
    Informar a que passo do fluxo de eventos deste caso de uso o relatório em questão está vinculado.
-->

  

#### Leiaute

<!--
    Incluir as imagens que representam o layout do relatório. Indicar, caso exista, o padrão utilizado pelo TSE.
-->

  

#### Informações do relatório

<!--
    A tabela abaixo define as informações que deverão constar do relatório. As seguintes propriedades devem ser definidas:
    Nome: Título do campo.
    Descrição: Descrição do campo.
    Tipo de dado: Incluir na célula uma das seguintes opções: Numérico; Alfanumérico; Data; Botão de Ação ou Outro (nesta opção é necessária a documentação de alguma regra vinculada).
    Tamanho: Tamanho do campo, no formato NN.
    Formatação: Formatação (máscara) do campo, por exemplo: campos do tipo data podem ser formatados como “dd/mm/aaaa”, “dd/mm”, “dd.mm.aa”, etc.
-->

<!-- Tabela -->

| **Nome**                        | **Descrição**                                                                                                     | **Tipo**      | **Tamanho** | **Formatação**   |
| ------------------------------- | ----------------------------------------------------------------------------------------------------------------- | ------------- | ----------- | ---------------- |
| **[Título / nome do campo1]**   | [*Descrição clara e completa do campo e sua origem: Ex.: “CNPJ do Fornecedor associado ao Material informado”*]   | [*N,A,D,O*]   | Tam1        | [*N, A, D, O*]   |
| **[Título / nome do campo2]**   | Des2                                                                                                              | Tip2          | Tam2        | For2             |
| **[Título / nome do campo3]**   | Des3                                                                                                              | Tip3          | Tam3        | For3             |
| **[Título / nome do campo4]**   | Des4                                                                                                              | Tip4          | Tam4        | For4             |
| **[Título / nome do campo5]**   | Des5                                                                                                              | Tip5          | Tam5        | For5             |
| **[Título / nome do campo6]**   | Des6                                                                                                              | Tip6          | Tam6        | For6             |
| **[Título / nome do campo7]**   | Des7                                                                                                              | Tip7          | Tam7        | For7             |

  
  

> SEPROP <<seprop@tse.jus.br>> Sobre o *template* Markdown - CDU: versão 1.0/ 01.03.2018 Url do *template* de referência: <http://sticonhecimento.tse.jus.br/cogti/seprop/templates/modelos-de-documentos-do-produs>
