![float](../../01_docs/00_imagem_template/Produs_git.png)

## PRODUS - Processo de Desenvolvimento Unificado de *Software*
### ERN - Especificação de Regras de Negócio

<br/>

## 1. Introdução

A finalidade deste documento é descrever as regras que são aplicáveis ao negócio. As regras de negócio são declarações sobre políticas ou condições que devem ser satisfeitas pelo sistema [*Nome do sistema*].

## 2. Escopo

Este documento refere-se apenas ao processo organizacional (negócio) suportado pelo sistema [*Nome do sistema*].

## 3. Sumário de regras

<!--
    Listar as regras, agrupadas ou não, fazendo link direto para facilitar o acesso.
    
    Excluir o item abaixo (opcional) se não aplicável. Observar a numeração dos tópcos e ajustar se for o caso
-->

### 4.1 Nome do Grupo de Regras (Opcional)

<!--
    RN001 – Título da regra 001 +
    RN002 – Título da regra 002 +
    RN00N – Título da regra 00N +
-->

<!--
    Excluir o item abaixo (opcional) se não aplicável. Observar a numeração dos tópicos e ajustar se for o caso
-->

### 4.2 Nome do Grupo de Regras (Opcional)

<!--
    RN003 – Título da regra 003 +
    RN004 – Título da regra 004 +
    RN00N – Título da regra 00N +
-->


## 4. Regras de Negócio (RN)

<!--
    Descreva as políticas ou condições organizacionais que devem ser satisfeitas pelo sistema.
    
    Atenção!
    Definição sobre regra de negócio a ser considerada para a elaboração do documento: [Uma regra de negócio representa um requisito de como os negócios, incluindo suas ferramentas de negócios devem operar. Exemplificando, esses requisitos podem ser provenientes de leis, portarias, regulamentos, medidas provisórias, atos declaratórios, normas, instruções normativas, estatutos, referências bibliográficas relevantes, decisões da gestão do Tribunal, decisões da gestão de projeto, costumes, senso comum, entre outros.
    
    Sobre o teor do documento:Outras regras que não se enquadrarem no conceito supracitado, tais como regras de domínio, regras de apresentação de interface gráfica etc., não devem constar neste documento.
    
    Sobre o preenchimento do documento:
    1. Nome do grupo de regras (agrupador opcional) – para uma melhor organização e entendimento, recomenda-se que as regras sejam agrupadas por assunto. Nesse caso, forneça uma breve descrição sobre o que o grupo representa;
    2. Número da regra de negócio (campo “Nº.”) – ao longo de todo o documento, para cada regra, utilize um identificador único no seguinte formato: prefixo RN + número sequencial;
    3. Título da regra de negócio (campo “Nome da regra”) – para cada regra, apresente um título significativo de modo que o leitor possa formar uma convicção sobre o conteúdo da regra a ser detalhado a seguir.
    
    Excluir o item abaixo (opcional) se não aplicável. Observar a numeração dos tópicos e ajustar se for o caso
-->

### 4.1 Nome do Grupo de Regras (Opcional)

#### RN001 – Título da regra 001

  - **Decrição**:

<!-- end list -->

<!--
    Descreva a regra de negócio.
-->

  - **Itens relacionados**:

<!-- end list -->

<!--
    Caso aplicável, referencie itens relacionados, como, por exemplo, outras regras de negócio, casos de uso, identificadores de mensagens do sistema, etc.
-->

#### RN002 – Título da regra 002

  - **Decrição**:

<!-- end list -->

<!--
    Descreva a regra de negócio.
-->

  - **Itens relacionados**:

<!-- end list -->

<!--
    Caso aplicável, referencie itens relacionados, como, por exemplo, outras regras de negócio, casos de uso, identificadores de mensagens do sistema, etc.
-->

#### RN00N – Título da regra 00N

  - **Decrição**:

<!-- end list -->

<!--
    Descreva a regra de negócio.
-->

  - **Itens relacionados**:

<!-- end list -->

<!--
    Caso aplicável, referencie itens relacionados, como, por exemplo, outras regras de negócio, casos de uso, identificadores de mensagens do sistema, etc.
-->

<!--
    Excluir o item abaixo (opcional) se não aplicável. Observar a numeração dos tópicos e ajustar se for o caso
-->

### 4.2 Nome do Grupo de Regras (Opcional)

#### RN003 – Título da regra 003

  - **Decrição**:

<!-- end list -->

<!--
    Descreva a regra de negócio.
-->

  - **Itens relacionados**:

<!-- end list -->

<!--
    Caso aplicável, referencie itens relacionados, como, por exemplo, outras regras de negócio, casos de uso, identificadores de mensagens do sistema, etc.
-->

#### RN004 – Título da regra 004

  - **Decrição**:

<!-- end list -->

<!--
    Descreva a regra de negócio.
-->

  - **Itens relacionados**:

<!-- end list -->

<!--
    Caso aplicável, referencie itens relacionados, como, por exemplo, outras regras de negócio, casos de uso, identificadores de mensagens do sistema, etc.
-->

#### RN00N – Título da regra 00N

  - **Decrição**:  

<!-- end list -->

<!--
    Descreva a regra de negócio.
-->

  - **Itens relacionados**:

<!-- end list -->

<!--
    Caso aplicável, referencie itens relacionados, como, por exemplo, outras regras de negócio, casos de uso, identificadores de mensagens do sistema, etc.
-->

### 4. Referências

<!--
    Essa subseção deve fornecer uma lista completa de todos os documentos referenciados em qualquer ponto da especificação de regras de negócio. Especificar as fontes em que as referências podem ser obtidas.
-->

> SEPROP <<seprop@tse.jus.br>> Sobre o *template* Markdown - ERN: versão 1.00/ 02.03.2018 Url do *template* de referência: <http://sticonhecimento.tse.jus.br/cogti/seprop/templates/modelos-de-documentos-do-produs>
