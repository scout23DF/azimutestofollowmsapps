package br.com.dokmossolucoes.azimutestofollow.person.repository;

import br.com.dokmossolucoes.azimutestofollow.person.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
