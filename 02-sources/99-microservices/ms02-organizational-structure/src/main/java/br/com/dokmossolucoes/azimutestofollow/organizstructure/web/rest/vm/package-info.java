/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.dokmossolucoes.azimutestofollow.organizstructure.web.rest.vm;
