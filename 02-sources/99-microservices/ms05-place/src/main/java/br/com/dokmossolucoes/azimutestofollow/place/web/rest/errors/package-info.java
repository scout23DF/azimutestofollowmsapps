/**
 * Specific errors used with Zalando's "problem-spring-web" library.
 *
 * More information on https://github.com/zalando/problem-spring-web
 */
package br.com.dokmossolucoes.azimutestofollow.place.web.rest.errors;
