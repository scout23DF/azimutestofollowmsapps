/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.dokmossolucoes.azimutestofollow.place.web.rest.vm;
