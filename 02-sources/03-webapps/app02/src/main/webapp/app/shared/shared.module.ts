import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { AzimstofollowGateway02SharedLibsModule, AzimstofollowGateway02SharedCommonModule, HasAnyAuthorityDirective } from './';

@NgModule({
    imports: [AzimstofollowGateway02SharedLibsModule, AzimstofollowGateway02SharedCommonModule],
    declarations: [HasAnyAuthorityDirective],
    providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }],
    exports: [AzimstofollowGateway02SharedCommonModule, HasAnyAuthorityDirective],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AzimstofollowGateway02SharedModule {}
