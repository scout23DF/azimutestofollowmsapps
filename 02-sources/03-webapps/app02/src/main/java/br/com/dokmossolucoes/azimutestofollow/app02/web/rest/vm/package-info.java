/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.dokmossolucoes.azimutestofollow.app02.web.rest.vm;
