/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.dokmossolucoes.azimutestofollow.app01.web.rest.vm;
