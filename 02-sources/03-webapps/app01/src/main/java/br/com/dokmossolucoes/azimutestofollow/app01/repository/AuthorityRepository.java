package br.com.dokmossolucoes.azimutestofollow.app01.repository;

import br.com.dokmossolucoes.azimutestofollow.app01.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
