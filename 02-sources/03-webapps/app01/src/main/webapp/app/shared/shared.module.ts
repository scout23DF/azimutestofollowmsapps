import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { AzimstofollowGatewayWebapp01SharedLibsModule, AzimstofollowGatewayWebapp01SharedCommonModule, HasAnyAuthorityDirective } from './';

@NgModule({
    imports: [AzimstofollowGatewayWebapp01SharedLibsModule, AzimstofollowGatewayWebapp01SharedCommonModule],
    declarations: [HasAnyAuthorityDirective],
    providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }],
    exports: [AzimstofollowGatewayWebapp01SharedCommonModule, HasAnyAuthorityDirective],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AzimstofollowGatewayWebapp01SharedModule {}
