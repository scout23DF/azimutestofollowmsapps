import { NgModule } from '@angular/core';

import { AzimstofollowGatewayWebapp01SharedLibsModule, FindLanguageFromKeyPipe, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [AzimstofollowGatewayWebapp01SharedLibsModule],
    declarations: [FindLanguageFromKeyPipe, JhiAlertComponent, JhiAlertErrorComponent],
    exports: [AzimstofollowGatewayWebapp01SharedLibsModule, FindLanguageFromKeyPipe, JhiAlertComponent, JhiAlertErrorComponent]
})
export class AzimstofollowGatewayWebapp01SharedCommonModule {}
