/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.dokmossolucoes.azimutestofollow.aztofollowwebapp.web.rest.vm;
